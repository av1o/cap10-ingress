# CAP10 Ingress Controller

The Cap10 Ingress Controller aims to build off the Cap10 project and provide a simple, "It Just Works" experience for Ingress.

Progress:
* [x] Ingress API support
* [x] IngressClass API support
* [x] Non HTTP backends (partial - supports `HTTP,HTTPS,H2C`)
  * [ ] Support for mTLS backends
* [x] Support for non-standard cluster domains
* [x] HTTP -> HTTPS redirect
* [x] Server port lookups
* [x] TLS configuration (partial)
  * [x] Server certificate hot-loading
  * [x] Client certificate hot-loading
  * [ ] Configurable `tls.Config`
* [ ] Named port caching
* [ ] Authentication
  * [x] mTLS
  * [ ] OIDC
* [ ] Prometheus metrics
* [x] Tracing
* [ ] Support for upstream TLS termination (e.g. AWS ELB)
* [ ] Plugins