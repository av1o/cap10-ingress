package main

import (
	"context"
	_ "embed"
	"fmt"
	"github.com/djcass44/go-tracer/tracer"
	"github.com/gorilla/mux"
	"github.com/kelseyhightower/envconfig"
	"gitlab.com/av1o/cap10-ingress/internal/certs"
	config2 "gitlab.com/av1o/cap10-ingress/internal/config"
	"gitlab.com/av1o/cap10-ingress/internal/k8s/config"
	"gitlab.com/av1o/cap10-ingress/internal/k8s/ingress"
	"gitlab.com/av1o/cap10-ingress/internal/k8s/secret"
	"gitlab.com/av1o/cap10-ingress/internal/k8s/service"
	"gitlab.com/av1o/cap10-ingress/internal/metrics"
	"gitlab.com/av1o/cap10-ingress/internal/routing"
	"gitlab.com/av1o/cap10-ingress/internal/server"
	"gitlab.com/av1o/cap10-ingress/pkg/logging"
	"gitlab.com/av1o/cap10-ingress/pkg/sni"
	"gitlab.com/av1o/cap10-ingress/pkg/tcputils"
	"gitlab.com/av1o/cap10-ingress/pkg/tracing"
	"go.opentelemetry.io/contrib/instrumentation/github.com/gorilla/mux/otelmux"
	_ "go.uber.org/automaxprocs"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"net"
	"net/http"
)
import log "github.com/sirupsen/logrus"

//go:embed banner.txt
var bannerText string

type environment struct {
	Server      server.Config
	Scope       string
	Log         logging.Config
	ConfigPath  string `split_words:"true"`
	Environment string `split_words:"true" envconfig:"GITLAB_ENVIRONMENT_NAME"`

	Metrics struct {
		Enabled bool `split_words:"true"`
		Port    int  `split_words:"true" default:"10250"`
	}

	Kubernetes struct {
		PodName   string `split_words:"true"`
		Namespace string `split_words:"true"`
	}
}

func main() {
	// read config
	var e environment
	err := envconfig.Process("cap10", &e)
	if err != nil {
		log.WithError(err).Fatal("failed to read config")
		return
	}

	// configure logging
	logging.Init(&e.Log)

	// read the config
	conf, err := config2.Read(e.ConfigPath)
	if err != nil {
		log.WithError(err).Fatal("failed to read load config")
		return
	}

	// setup otel
	if conf.OpenTelemetry.Enabled {
		err := tracing.Init(tracing.OtelOptions{
			ServiceName: tracing.DefaultServiceName,
			Environment: e.Environment,
			SampleRate:  conf.OpenTelemetry.SampleRate,
		})
		if err != nil {
			log.WithError(err).Error("failed to setup tracing")
		}
	}

	// start the tcp proxy
	tcpCol := tcputils.NewTCPProxyCollector(e.Kubernetes.PodName, e.Kubernetes.Namespace)
	if err := tcputils.NewProxiesFromConfig(conf.Kube.ClusterDomain, conf.TCP, tcpCol); err != nil {
		log.WithError(err).Fatal("failed to setup tcp proxy due to config error")
		return
	}

	// setup config
	kubeconfig, err := rest.InClusterConfig()
	if err != nil {
		log.WithError(err).Fatal("failed to fetch in-cluster config")
		return
	}
	clientset, err := kubernetes.NewForConfig(kubeconfig)
	if err != nil {
		log.WithError(err).Fatal("failed to build client from config")
		return
	}
	// fetch the IngressClass
	class, err := ingress.NewReader(clientset).RetryGetClass(context.Background(), conf.IngressClassName)
	if err != nil {
		log.WithError(err).Fatalf("failed to fetch IngressClass: '%s'", conf.IngressClassName)
		return
	}

	// set up services
	res := config.NewResolver(service.NewReader(clientset))
	hotLoader, err := certs.NewHotLoader(secret.NewReader(clientset), conf.TLS.DefaultCertificate)
	if err != nil {
		log.WithError(err).Fatal("failed to setup tls hot-loader")
		return
	}

	// start watching ingresses
	stopper := ingress.Init(
		e.Scope,
		clientset,
		[]ingress.ChangeListener{res, hotLoader, ingress.NewStatusListener(clientset)},
		class.Name,
		class.ObjectMeta.Annotations[ingress.DefaultClassAnnotation] == "true",
	)
	defer close(stopper)

	// configure http
	router := mux.NewRouter()
	router.Use(tracer.NewHandler)
	if conf.OpenTelemetry.Enabled {
		router.Use(otelmux.Middleware(tracing.DefaultServiceName))
	}
	router.HandleFunc("/healthz", func(w http.ResponseWriter, r *http.Request) {
		log.WithFields(log.Fields{
			"userAgent":  r.UserAgent(),
			"remoteAddr": r.RemoteAddr,
		}).Debug("answering probe")
		_, _ = w.Write([]byte("OK"))
	}).
		Methods(http.MethodGet).
		HeadersRegexp("User-Agent", "kube-probe/*")

	servers := server.NewSrv(&e.Server, router, hotLoader)

	l, err := net.Listen("tcp", fmt.Sprintf(":%d", e.Server.Secure))
	if err != nil {
		log.WithError(err).Fatal("failed to open TLS listener")
		return
	}

	tlsListener := sni.MuxListener(l, func(host string) (*sni.ProxyConfig, bool) {
		svc, _, err := res.GetSNI(context.TODO(), host)
		if err != nil {
			return nil, false
		}
		return &sni.ProxyConfig{
			Addr: svc.String(conf.Kube.ClusterDomain),
		}, true
	})

	// configure the primary router
	var h3Headers routing.AltHeaderConfigurer
	if servers.Srv3 != nil {
		h3Headers = servers.Srv3.SetQuicHeaders
	}
	routingCol := routing.NewProxyCollector(e.Kubernetes.PodName, e.Kubernetes.Namespace, class.Name)
	router.PathPrefix("/").Handler(routing.NewCapRouter(res, conf.Kube.ClusterDomain, &conf.HSTS, &conf.Transport, routingCol, h3Headers, conf.AllowSNISmuggling))

	// enable metrics exporting
	if e.Metrics.Enabled {
		mSrv := metrics.NewCollector(e.Metrics.Port, routingCol, tcpCol)
		go mSrv.Serve()
	}

	// start up
	fmt.Println(bannerText)
	servers.Serve(tlsListener)
}
