package testutils

import "context"

type PortResolver struct{}

func (*PortResolver) GetPortByName(context.Context, string, string, string) (int32, error) {
	return 8080, nil
}
