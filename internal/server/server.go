package server

import (
	"crypto/tls"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/lucas-clemente/quic-go/http3"
	log "github.com/sirupsen/logrus"
	"gitlab.com/av1o/cap10-ingress/internal/certs"
	"io"
	stdlog "log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

type Config struct {
	Secure    int  `split_words:"true" default:"8443"`
	HTTP3     bool `envconfig:"CAP10_SERVER_HTTP3" default:"false"`
	HTTP3Port int  `envconfig:"CAP10_SERVER_HTTP3_PORT" default:"443"`
	Insecure  int  `split_words:"true" default:"8080"`
}

type Srv struct {
	Srv1 *http.Server
	Srv2 *http.Server
	Srv3 *http3.Server
}

func NewSrv(cfg *Config, router http.Handler, loader *certs.HotLoader) *Srv {
	// create a logrus logger that we
	// can write to
	logger := log.New()
	w := logger.WriterLevel(log.ErrorLevel)
	defer w.Close()
	addr := fmt.Sprintf(":%d", cfg.Secure)
	srv := &http.Server{
		Addr:     addr,
		Handler:  router,
		ErrorLog: stdlog.New(w, "", 0),
		TLSConfig: &tls.Config{
			PreferServerCipherSuites: true,
			MinVersion:               tls.VersionTLS12,
			CipherSuites: []uint16{
				tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
				tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
				tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
				tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
				tls.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305,
				tls.TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,
			},
		},
	}
	// inject what we need so that we can hot-reload
	// the certificates
	srv.TLSConfig.GetCertificate = loader.GetCertificateFunc()
	srv.TLSConfig.GetConfigForClient = loader.GetConfigForClientFunc()
	return &Srv{
		Srv1: getHTTPServer(cfg, w),
		Srv2: srv,
		Srv3: &http3.Server{
			Server: srv,
			Port:   uint32(cfg.HTTP3Port),
		},
	}
}

// Serve starts the HTTP server and blocks until
// it exists, or we receive a kill signal
func (s *Srv) Serve(tlsListener net.Listener) {
	// start the server
	go func() {
		log.WithField("addr", s.Srv1.Addr).Info("starting insecure server")
		log.WithError(s.Srv1.ListenAndServe()).Fatal("insecure server has exited")
	}()
	go func() {
		if s.Srv3 != nil {
			log.WithField("addr", s.Srv3.Addr).Info("starting secure http/3 server")
			conn, err := net.ListenPacket("udp", s.Srv3.Addr)
			if err != nil {
				log.WithError(err).Fatal("failed to start http/3 listener")
				return
			}
			// start the server in the background
			go func() {
				log.WithError(s.Srv3.Serve(conn)).Fatal("secure server has exited")
			}()
		}
		log.WithField("addr", s.Srv2.Addr).Info("starting secure http/2 server")
		log.WithError(s.Srv2.ServeTLS(tlsListener, "", "")).Fatal("secure server has exited")
	}()

	// wait for a signal
	sigC := make(chan os.Signal, 1)
	signal.Notify(sigC, syscall.SIGTERM, syscall.SIGINT)
	sig := <-sigC
	log.Infof("received SIGTERM/SIGINT (%s), shutting down...", sig)
}

// getHTTPServer creates a http server with the sole purpose
// of issuing a http -> https redirect and answering
// kubernetes health checks
func getHTTPServer(cfg *Config, w io.Writer) *http.Server {
	addr := fmt.Sprintf(":%d", cfg.Insecure)
	router := mux.NewRouter()
	// handle health checks
	router.HandleFunc("/healthz", func(w http.ResponseWriter, r *http.Request) {
		log.WithFields(log.Fields{
			"userAgent":  r.UserAgent(),
			"remoteAddr": r.RemoteAddr,
		}).Debug("answering probe")
		_, _ = w.Write([]byte("OK"))
	}).
		Methods(http.MethodGet).
		HeadersRegexp("User-Agent", "kube-probe/*")
	// otherwise, redirect the user to HTTPS
	router.PathPrefix("/").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, fmt.Sprintf("https://%s%s", r.Host, r.RequestURI), http.StatusPermanentRedirect)
	})
	return &http.Server{
		Addr:         addr,
		Handler:      router,
		ReadTimeout:  60 * time.Second,
		WriteTimeout: 60 * time.Second,
		ErrorLog:     stdlog.New(w, "", 0),
	}
	//log.WithField("addr", addr).Info("starting insecure server")
	//log.WithError(srv.ListenAndServe()).Fatal("insecure server has exited")
}
