package server

import (
	"crypto/tls"
	"fmt"
	"github.com/lucas-clemente/quic-go/http3"
	"github.com/stretchr/testify/assert"
	"gitlab.com/av1o/cap10-ingress/internal/certs"
	"k8s.io/apimachinery/pkg/util/rand"
	"net"
	"net/http"
	"os"
	"testing"
	"time"
)

//func TestServe2(t *testing.T) {
//	cl, err := certs.NewHotLoader(nil, "")
//	assert.NoError(t, err)
//	portInsecure := rand.IntnRange(30000, 50000)
//	router := mux.NewRouter()
//	srv := NewSrv(&Config{
//		Secure:   8443,
//		HTTP3:    true,
//		Insecure: portInsecure,
//	}, router, cl)
//	router.PathPrefix("/").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
//		_ = srv.Srv3.SetQuicHeaders(w.Header())
//		_, _ = w.Write([]byte("OK"))
//	})
//	srv.Serve()
//}

func TestServe(t *testing.T) {
	cl, err := certs.NewHotLoader(nil, "")
	assert.NoError(t, err)
	port := rand.IntnRange(30000, 50000)
	portInsecure := rand.IntnRange(30000, 50000)

	l, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	assert.NoError(t, err)

	go NewSrv(&Config{
		Secure:   port,
		HTTP3:    true,
		Insecure: portInsecure,
	}, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, _ = w.Write([]byte("OK"))
	}), cl).Serve(l)

	var cases = []struct {
		name   string
		client *http.Client
	}{
		{
			"http/3 client connects",
			&http.Client{
				Transport: &http3.RoundTripper{
					TLSClientConfig: &tls.Config{
						InsecureSkipVerify: true,
					},
				},
			},
		},
		{
			"http/2 client fails",
			&http.Client{
				Transport: &http.Transport{
					TLSClientConfig: &tls.Config{
						InsecureSkipVerify: true,
					},
					ForceAttemptHTTP2: true,
				},
			},
		},
		{
			"http/1 client fails",
			&http.Client{
				Transport: &http.Transport{
					TLSClientConfig: &tls.Config{
						InsecureSkipVerify: true,
					},
					ForceAttemptHTTP2: false,
				},
			},
		},
	}

	time.Sleep(time.Second)

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			resp, err := tt.client.Get(fmt.Sprintf("https://localhost:%d", port))
			assert.NoError(t, err)
			assert.EqualValues(t, http.StatusOK, resp.StatusCode)
		})
	}
}

func TestServerInsecure(t *testing.T) {
	c := &http.Client{
		CheckRedirect: func(*http.Request, []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}

	port := rand.IntnRange(1025, 65534)
	go func() {
		_ = getHTTPServer(&Config{
			Secure:   0,
			Insecure: port,
		}, os.Stdout).ListenAndServe()
	}()
	time.Sleep(time.Second)
	resp, err := c.Get(fmt.Sprintf("http://localhost:%d", port))
	assert.NoError(t, err)
	assert.NoError(t, resp.Body.Close())
	assert.EqualValues(t, http.StatusPermanentRedirect, resp.StatusCode)
}
