package certs

import (
	"context"
	"crypto/tls"
	_ "embed"
	"github.com/stretchr/testify/assert"
	"gitlab.com/av1o/cap10-ingress/internal/k8s/secret"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes/fake"
	"testing"
)

//go:embed testdata/ca.crt
var caCert []byte

func TestGetConfigRule(t *testing.T) {
	client := fake.NewSimpleClientset(&v1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "test",
			Namespace: "default",
		},
		Data: map[string][]byte{
			secret.KeyCA: caCert,
		},
		Type: v1.SecretTypeTLS,
	})
	reader := secret.NewReader(client)

	t.Run("default values requests no cert", func(t *testing.T) {
		rule, err := GetConfigRule(context.TODO(), map[string]string{}, nil)
		assert.NoError(t, err)
		assert.NotNil(t, rule)
		assert.Nil(t, rule.clientAuth)
		assert.Nil(t, rule.clientCAs)
	})
	t.Run("config but no secret returns error", func(t *testing.T) {
		rule, err := GetConfigRule(context.TODO(), map[string]string{
			"nginx.ingress.kubernetes.io/auth-tls-verify-client": "on",
		}, reader)
		assert.Nil(t, rule)
		assert.True(t, errors.IsNotFound(err))
	})
	t.Run("config returns success", func(t *testing.T) {
		rule, err := GetConfigRule(context.TODO(), map[string]string{
			"nginx.ingress.kubernetes.io/auth-tls-verify-client": "optional",
			"nginx.ingress.kubernetes.io/auth-tls-secret":        "default/test",
		}, reader)
		assert.NotNil(t, rule)
		assert.NoError(t, err)
		assert.EqualValues(t, tls.RequestClientCert, *rule.clientAuth)
		assert.NotNil(t, rule.clientCAs)
	})
	t.Run("optional_no_ca config returns success but no cert pool", func(t *testing.T) {
		rule, err := GetConfigRule(context.TODO(), map[string]string{
			"nginx.ingress.kubernetes.io/auth-tls-verify-client": "optional_no_ca",
			"nginx.ingress.kubernetes.io/auth-tls-secret":        "default/test",
		}, reader)
		assert.NotNil(t, rule)
		assert.NoError(t, err)
		assert.EqualValues(t, tls.RequestClientCert, *rule.clientAuth)
		assert.Nil(t, rule.clientCAs)
	})
}
