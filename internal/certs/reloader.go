package certs

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"github.com/djcass44/go-utils/pkg/sliceutils"
	log "github.com/sirupsen/logrus"
	"gitlab.com/av1o/cap10-ingress/internal/k8s/secret"
	"gitlab.com/av1o/cap10-ingress/pkg/k8s"
	"gitlab.com/av1o/cap10-ingress/pkg/tracing"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
	v1 "k8s.io/api/networking/v1"
	"strings"
)

type CertificateRule struct {
	certificate *tls.Certificate
	hosts       []string
}

type HotLoader struct {
	secretReader *secret.Reader
	// stores server configuration
	certificates     map[string][]*CertificateRule
	certificateCache map[string]*tls.Certificate
	// stores client configuration
	configs     map[string]*ConfigRule
	configCache map[string]*tls.Config
	// default server cert to use if
	// the user didn't specify one
	defaultCert *tls.Certificate
}

func NewHotLoader(secretReader *secret.Reader, defaultSecret string) (*HotLoader, error) {
	hl := new(HotLoader)
	hl.secretReader = secretReader
	hl.certificates = map[string][]*CertificateRule{}
	hl.certificateCache = map[string]*tls.Certificate{}
	hl.configs = map[string]*ConfigRule{}
	hl.configCache = map[string]*tls.Config{}

	if defaultSecret != "" {
		log.Infof("reading default certificate from secret: %s", defaultSecret)
		// try to read the default secret
		bits := strings.Split(defaultSecret, "/")
		cert, err := secretReader.ReadSecret(context.Background(), bits[1], bits[0])
		if err != nil {
			log.WithError(err).Errorf("failed to read default secret: %s", defaultSecret)
			return nil, err
		}
		hl.defaultCert = cert
	} else {
		log.Warning("no default certificate has been specified, we will have to self sign :(")
		cert, err := NewFakeCertificate()
		if err != nil {
			return nil, err
		}
		hl.defaultCert = cert
	}

	return hl, nil
}

func (hl *HotLoader) Add(ctx context.Context, ing *v1.Ingress) {
	ctx, span := otel.Tracer(tracing.DefaultServiceName).Start(ctx, "listener_tls_add", trace.WithAttributes(
		attribute.String("name", ing.GetName()),
		attribute.String("namespace", ing.GetNamespace()),
	))
	defer span.End()
	fields := log.Fields{
		"uid":       ing.GetUID(),
		"namespace": ing.GetNamespace(),
		"name":      ing.GetName(),
	}
	log.WithFields(fields).WithContext(ctx).Debug("reloading certs")
	clientConfig, _ := GetConfigRule(context.TODO(), ing.ObjectMeta.Annotations, hl.secretReader)
	secrets := map[string][]string{}
	// fetch the list of secrets and the hosts they serve
	for _, t := range ing.Spec.TLS {
		secrets[fmt.Sprintf("%s/%s", ing.GetNamespace(), t.SecretName)] = t.Hosts
	}
	var certs []*CertificateRule //nolint:prealloc
	for cert, hosts := range secrets {
		// drop parts of the cache
		for _, h := range hosts {
			// only set the config if it's valid
			if clientConfig != nil && clientConfig.clientAuth != nil {
				hl.configs[h] = clientConfig
			}
			delete(hl.certificateCache, h)
		}
		// read the cert
		obj := k8s.NewScopedResource(cert)
		kp, err := hl.secretReader.ReadSecret(context.TODO(), obj.Name, obj.Namespace)
		if err != nil {
			continue
		}
		// add it to our list
		certs = append(certs, &CertificateRule{
			certificate: kp,
			hosts:       hl.filterInvalid(ctx, kp, hosts),
		})
	}
	log.WithFields(fields).WithContext(ctx).Debugf("successfully reloaded %d certs", len(certs))
	hl.certificates[string(ing.GetUID())] = certs
}

func (hl *HotLoader) Delete(ctx context.Context, ing *v1.Ingress) {
	ctx, span := otel.Tracer(tracing.DefaultServiceName).Start(ctx, "listener_tls_delete", trace.WithAttributes(
		attribute.String("name", ing.GetName()),
		attribute.String("namespace", ing.GetNamespace()),
	))
	defer span.End()
	fields := log.Fields{
		"uid":       ing.GetUID(),
		"namespace": ing.GetNamespace(),
		"name":      ing.GetName(),
	}
	log.WithContext(ctx).WithFields(fields).Debug("deleting certs")
	// drop cache
	count := 0
	for _, t := range ing.Spec.TLS {
		for _, h := range t.Hosts {
			delete(hl.certificateCache, h)
			delete(hl.configs, h)
			delete(hl.configCache, h)
			count++
		}
	}
	log.WithFields(fields).WithContext(ctx).Debugf("successfully removed %d certs from cache", count)
	delete(hl.certificates, string(ing.GetUID()))
}

func (hl *HotLoader) GetCertificateFunc() func(*tls.ClientHelloInfo) (*tls.Certificate, error) {
	return func(info *tls.ClientHelloInfo) (*tls.Certificate, error) {
		log.WithContext(info.Context()).Debugf("received clienthello addressed to: %s", info.ServerName)
		return hl.getCertificate(info.ServerName), nil
	}
}

func (hl *HotLoader) GetConfigForClientFunc() func(info *tls.ClientHelloInfo) (*tls.Config, error) {
	return func(info *tls.ClientHelloInfo) (*tls.Config, error) {
		return hl.getTLSConfig(info.ServerName), nil
	}
}

func (hl *HotLoader) getCertificate(serverName string) *tls.Certificate {
	fields := log.Fields{"sni": serverName}
	if serverName == "" {
		log.WithFields(fields).Debugf("missing sni, using default certificate")
		return hl.defaultCert
	}
	if cert, ok := hl.certificateCache[serverName]; ok {
		log.WithFields(fields).Debug("located cached cert")
		return cert
	}
	log.WithFields(fields).Debug("failed to locate cached cert, doing slow lookup")
	for _, rule := range hl.certificates {
		for _, cert := range rule {
			if sliceutils.Includes(cert.hosts, serverName) {
				// populate the cache
				hl.certificateCache[serverName] = cert.certificate
				return cert.certificate
			}
		}
	}
	log.WithFields(fields).Debug("failed to find cert, using default certificate")
	hl.certificateCache[serverName] = hl.defaultCert
	return hl.defaultCert
}

func (hl *HotLoader) getTLSConfig(serverName string) *tls.Config {
	fields := log.Fields{"sni": serverName}
	if serverName == "" {
		log.WithFields(fields).Debugf("missing sni, using default config")
		return nil
	}
	// see if we can fetch something from the cache
	if conf, ok := hl.configCache[serverName]; ok {
		log.WithFields(fields).Debug("located cached tls config")
		return conf
	}
	log.WithFields(fields).Debug("failed to locate cached tls config, doing slow lookup")
	// otherwise, we'll need to create a new tls config
	conf, ok := hl.configs[serverName]
	if !ok || (ok && conf == nil) {
		log.WithFields(fields).Debug("we have no configuration, using server preference")
		return nil
	}
	log.WithFields(fields).Debug("generating new tls config")
	tlsConfig := &tls.Config{
		PreferServerCipherSuites: true,
		ClientCAs:                conf.clientCAs,
		ClientAuth:               *conf.clientAuth,
		MinVersion:               tls.VersionTLS12,
		GetCertificate:           hl.GetCertificateFunc(),
		CipherSuites: []uint16{
			tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
			tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
			tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305,
			tls.TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,
		},
	}
	// populate the cache
	hl.configCache[serverName] = tlsConfig
	return tlsConfig
}

// filterInvalid checks that the given certificate is
// valid for the named hosts.
//
// errors and failures are swallowed and will lead
// to an empty/nil slice being returned.
func (hl *HotLoader) filterInvalid(ctx context.Context, keyPair *tls.Certificate, hosts []string) []string {
	ctx, span := otel.Tracer(tracing.DefaultServiceName).Start(ctx, "listener_tls_filterInvalid", trace.WithAttributes(
		attribute.StringSlice("hosts", hosts),
	))
	defer span.End()
	cert, err := x509.ParseCertificate(keyPair.Certificate[0])
	if err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to parse x509 certificate")
		return nil
	}
	var valid []string //nolint:prealloc
	for _, h := range hosts {
		if err := cert.VerifyHostname(h); err != nil {
			log.WithError(err).WithContext(ctx).Errorf("cert failed hostname validation for: %s", h)
			continue
		}
		valid = append(valid, h)
	}
	log.WithContext(ctx).Debugf("certificate is valid for %d/%d hosts", len(valid), len(hosts))
	return valid
}
