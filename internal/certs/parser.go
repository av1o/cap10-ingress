package certs

import (
	"context"
	"crypto/tls"
	log "github.com/sirupsen/logrus"
	"gitlab.com/av1o/cap10-ingress/internal/k8s/secret"
	"gitlab.com/av1o/cap10-ingress/pkg/k8s"
	"gitlab.com/av1o/cap10-ingress/pkg/tags"
)

func GetConfigRule(ctx context.Context, annotations map[string]string, reader *secret.Reader) (*ConfigRule, error) {
	var tlsOptions AuthTLSOptions
	err := tags.Marshal(&tlsOptions, annotations)
	if err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to marshal annotation data")
		return nil, err
	}
	log.WithContext(ctx).Debugf("detected mtls options: %+v", tlsOptions)
	var authType tls.ClientAuthType
	switch tlsOptions.VerifyClient {
	default:
		log.WithContext(ctx).Warningf("unknown or unsupported tls verification type: '%s'", tlsOptions.VerifyClient)
		fallthrough
	case "":
		fallthrough
	case "off":
		return &ConfigRule{
			nil,
			nil,
		}, nil
	case "on":
		authType = tls.RequireAndVerifyClientCert
	case "optional":
		authType = tls.RequestClientCert
	case "optional_no_ca":
		authType = tls.RequestClientCert
		return &ConfigRule{
			&authType,
			nil,
		}, nil
	}
	obj := k8s.NewScopedResource(tlsOptions.SecretName)
	pool, err := reader.ReadCA(ctx, obj.Name, obj.Namespace)
	if err != nil {
		return nil, err
	}
	return &ConfigRule{
		clientAuth: &authType,
		clientCAs:  pool,
	}, nil
}
