package certs

import (
	"crypto/tls"
	"crypto/x509"
)

type AuthTLSOptions struct {
	SecretName   string `k8s:"nginx.ingress.kubernetes.io/auth-tls-secret,cap10.ingress.dcas.dev/auth-tls-secret"`
	VerifyClient string `k8s:"nginx.ingress.kubernetes.io/auth-tls-verify-client,cap10.ingress.dcas.dev/auth-tls-verify-client"`
}

type ConfigRule struct {
	clientAuth *tls.ClientAuthType
	clientCAs  *x509.CertPool
}
