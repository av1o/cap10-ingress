package certs

import (
	"bytes"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	log "github.com/sirupsen/logrus"
	"math/big"
	"time"
)

// NewFakeCertificate generates a tls certificate
// for the https server
func NewFakeCertificate() (*tls.Certificate, error) {
	log.Info("generating self-signed certificate")
	priv, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	if err != nil {
		log.WithError(err).Error("failed to generate private key")
		return nil, err
	}
	serialNumber, err := rand.Int(rand.Reader, new(big.Int).Lsh(big.NewInt(1), 128))
	if err != nil {
		log.WithError(err).Error("failed to generate serial number")
		return nil, err
	}
	// create a crappy cert
	template := &x509.Certificate{
		SerialNumber: serialNumber,
		Subject: pkix.Name{
			CommonName: "Ingress Fake Certificate",
		},
		NotBefore:             time.Now(),
		NotAfter:              time.Now().AddDate(1, 0, 0),
		KeyUsage:              x509.KeyUsageDigitalSignature,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		BasicConstraintsValid: true,
		DNSNames:              []string{"*.local"},
	}
	derBytes, err := x509.CreateCertificate(rand.Reader, template, template, priv.Public(), priv)
	if err != nil {
		log.WithError(err).Error("failed to create certificate")
		return nil, err
	}
	// save it in memory
	certOut := bytes.NewBuffer(nil)
	if err := pem.Encode(certOut, &pem.Block{Type: "CERTIFICATE", Bytes: derBytes}); err != nil {
		log.WithError(err).Error("failed to encode certificate")
		return nil, err
	}
	keyOut := bytes.NewBuffer(nil)
	privBytes, err := x509.MarshalPKCS8PrivateKey(priv)
	if err != nil {
		log.WithError(err).Error("unable to marshal private key")
		return nil, err
	}
	if err := pem.Encode(keyOut, &pem.Block{Type: "PRIVATE KEY", Bytes: privBytes}); err != nil {
		log.WithError(err).Error("failed to encode private key")
		return nil, err
	}
	// read the keypair
	pair, err := tls.X509KeyPair(certOut.Bytes(), keyOut.Bytes())
	if err != nil {
		log.WithError(err).Error("failed to parse x509 keypair")
		return nil, err
	}
	log.Info("successfully generated self-signed certificate")
	return &pair, nil
}
