package routing

import (
	"net/http"
	"time"
)

const (
	BackendHTTPS = "HTTPS"
	BackendHTTP  = "HTTP"
	BackendH2C   = "H2C"
)

const (
	SchemeHTTP  = "http"
	SchemeHTTPS = "https"
	SchemeH2C   = "h2c"
)

const (
	HeaderSubject        = "ssl-client-subject-dn"
	HeaderIssuer         = "ssl-client-issuer-dn"
	HeaderVerify         = "ssl-client-verify"
	HeaderSTS            = "Strict-Transport-Security"
	HeaderForwardedProto = "X-Forwarded-Proto"
)

type contextKey int

const (
	contextKeyInfo contextKey = iota
)

type contextInfo struct {
	Ingress   string
	Service   string
	Namespace string
	Start     time.Time
}

type AltHeaderConfigurer = func(hdr http.Header) error

type Options struct {
	UpstreamConnectTimeout int `k8s:"nginx.ingress.kubernetes.io/proxy-connect-timeout,cap10.ingress.dcas.dev/proxy-connect-timeout"`
	UpstreamTLSTimeout     int `k8s:"cap10.ingress.dcas.dev/proxy-tls-timeout"`
	UpstreamTimeout        int `k8s:"nginx.ingress.kubernetes.io/proxy-read-timeout,cap10.ingress.dcas.dev/proxy-read-timeout"`
	// we can't parse nginx byte representation just yet
	UpstreamReadBufferSize int `k8s:"cap10.ingress.dcas.dev/proxy-buffer-size"`
	// UpstreamProtocol defines how we should
	// communicate with the backend.
	// Supports HTTP (default), HTTPS, H2C.
	//
	// H2C will invalidate most Options
	UpstreamProtocol string `k8s:"nginx.ingress.kubernetes.io/backend-protocol,cap10.ingress.dcas.dev/backend-protocol"`
}

// DefaultOptions returns an Options configured
// with sensible defaults.
func DefaultOptions() *Options {
	return &Options{
		UpstreamConnectTimeout: 10,
		UpstreamTLSTimeout:     10,
		UpstreamTimeout:        120,
		UpstreamReadBufferSize: 0,
		UpstreamProtocol:       BackendHTTP,
	}
}
