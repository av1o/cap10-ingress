package routing

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/assert"
	config2 "gitlab.com/av1o/cap10-ingress/internal/config"
	"gitlab.com/av1o/cap10-ingress/internal/k8s/config"
	"golang.org/x/net/http2"
	"golang.org/x/net/http2/h2c"
	v1 "k8s.io/api/networking/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"net/http"
	"net/http/httptest"
	"testing"
)

type testResolver struct {
	Name      string
	Namespace string
	Port      int32
}

func (t *testResolver) GetAddress(context.Context, string, string) (*config.ServiceDescriptor, *v1.Ingress, error) {
	return config.NewServiceDescriptor(t.Name, t.Namespace, t.Port), &v1.Ingress{
		ObjectMeta: metav1.ObjectMeta{
			Name:      t.Name,
			Namespace: t.Namespace,
		},
	}, nil
}

func (t *testResolver) GetSNI(context.Context, string) (*config.ServiceDescriptor, *v1.Ingress, error) {
	return nil, nil, config.ErrNoUpstream
}

var router = &CapRouter{
	transportConfig: &config2.Transport{},
}

func TestCapRouter_ServeHTTP(t *testing.T) {
	router := NewCapRouter(
		&testResolver{
			Name:      "foo",
			Namespace: "foo-prod",
			Port:      8080,
		},
		"cluster.local",
		&config2.HSTS{},
		&config2.Transport{},
		NewProxyCollector("", "", ""),
		nil,
		false,
	)
	req := httptest.NewRequest(http.MethodGet, "https://foo.bar", nil)
	w := httptest.NewRecorder()
	router.ServeHTTP(w, req)
	t.Log(w.Code)
}

func TestCapRouter_getRoutableAddr(t *testing.T) {
	ctx := context.TODO()
	var cases = []struct {
		name     string
		addr     string
		backend  string
		expected string
	}{
		{
			"http works",
			"foo.bar.svc.cluster.local:8080",
			BackendHTTP,
			"http://foo.bar.svc.cluster.local:8080",
		},
		{
			"https works",
			"foo.bar.svc.cluster.local:8080",
			BackendHTTPS,
			"https://foo.bar.svc.cluster.local:8080",
		},
		{
			"case insensitive",
			"foo.bar.svc.cluster.local:8080",
			"https",
			"https://foo.bar.svc.cluster.local:8080",
		},
		{
			"defaults to HTTP",
			"foo.bar.svc.cluster.local:8080",
			"",
			"http://foo.bar.svc.cluster.local:8080",
		},
	}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			addr := router.getRoutableAddr(ctx, tt.addr, &Options{
				UpstreamProtocol: tt.backend,
			})
			assert.EqualValues(t, tt.expected, addr)
		})
	}
}

func TestCapRouter_getTransport(t *testing.T) {
	ctx := context.TODO()
	ts := httptest.NewServer(h2c.NewHandler(http.NotFoundHandler(), &http2.Server{}))
	defer ts.Close()

	var cases = []struct {
		name string
		opts Options
	}{
		{
			"default options",
			*DefaultOptions(),
		},
		{
			"nil options",
			Options{
				UpstreamConnectTimeout: 0,
				UpstreamTLSTimeout:     0,
				UpstreamTimeout:        0,
				UpstreamReadBufferSize: 0,
				UpstreamProtocol:       "",
			},
		},
		{
			"h2c",
			Options{
				UpstreamProtocol: BackendH2C,
			},
		},
	}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			c := ts.Client()
			c.Transport = router.getTransport(ctx, &tt.opts)

			resp, err := c.Get(ts.URL)
			assert.NoError(t, resp.Body.Close())
			assert.NoError(t, err)
			assert.EqualValues(t, http.StatusNotFound, resp.StatusCode)
			if tt.opts.UpstreamProtocol == BackendH2C {
				assert.EqualValues(t, 2, resp.ProtoMajor)
			}
		})
	}
}

func TestCapRouter_modifyResponse(t *testing.T) {
	ts := httptest.NewServer(http.NotFoundHandler())
	defer ts.Close()
	r := &CapRouter{
		transportConfig: &config2.Transport{},
		hstsConfig: &config2.HSTS{
			Enabled:           true,
			IncludeSubdomains: true,
			Preload:           true,
			MaxAge:            3153600,
		},
	}
	// create a fake response
	response, err := ts.Client().Get(ts.URL)
	assert.NoError(t, err)
	// check that there was no sts header pre-mutation
	assert.Empty(t, response.Header.Get(HeaderSTS))
	err = r.modifyResponse(response)
	assert.NoError(t, err)
	assert.EqualValues(t, "max-age=3153600; includeSubDomains; preload", response.Header.Get(HeaderSTS))

	defer response.Body.Close()
}

func TestCapRouter_hostMatchesSNI(t *testing.T) {
	ctx := context.TODO()
	var cases = []struct {
		host string
		sni  string
		out  bool
	}{
		{
			"argocd.dcas.dev:443",
			"argocd.dcas.dev",
			true,
		},
		{
			"foo.com",
			"bar.com",
			false,
		},
		{
			"foo.com",
			"foo.com",
			true,
		},
	}
	for _, tt := range cases {
		t.Run(fmt.Sprintf("%s/%s/%v", tt.host, tt.sni, tt.out), func(t *testing.T) {
			assert.EqualValues(t, tt.out, router.hostMatchesSNI(ctx, tt.host, tt.sni))
		})
	}
}
