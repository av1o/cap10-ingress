package routing

import (
	"context"
	"crypto/tls"
	"fmt"
	log "github.com/sirupsen/logrus"
	config2 "gitlab.com/av1o/cap10-ingress/internal/config"
	"gitlab.com/av1o/cap10-ingress/internal/k8s/config"
	"gitlab.com/av1o/cap10-ingress/pkg/tags"
	"gitlab.com/av1o/cap10-ingress/pkg/tracing"
	"go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
	"golang.org/x/net/http2"
	"net"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strconv"
	"strings"
	"time"
)

type CapRouter struct {
	resolver            config.ResolverInterface
	clusterDomain       string
	hstsConfig          *config2.HSTS
	transportConfig     *config2.Transport
	proxyMetrics        *ProxyCollector
	altHeaderConfigurer AltHeaderConfigurer
	allowSNISmuggling   bool
}

func NewCapRouter(resolver config.ResolverInterface, clusterDomain string, hstsConfig *config2.HSTS, transportConfig *config2.Transport, proxyMetrics *ProxyCollector, altHeaderConfigurer AltHeaderConfigurer, allowSNISmuggling bool) *CapRouter {
	return &CapRouter{
		resolver:            resolver,
		clusterDomain:       clusterDomain,
		hstsConfig:          hstsConfig,
		transportConfig:     transportConfig,
		proxyMetrics:        proxyMetrics,
		altHeaderConfigurer: altHeaderConfigurer,
		allowSNISmuggling:   allowSNISmuggling,
	}
}

func (*CapRouter) hostMatchesSNI(ctx context.Context, host, sni string) bool {
	ctx, span := otel.Tracer(tracing.DefaultServiceName).Start(ctx, "router_cap_hostMatchesSNI", trace.WithAttributes(
		attribute.String("host", host),
		attribute.String("sni", sni),
	))
	defer span.End()
	if host == sni {
		span.AddEvent("host and sni exactly match")
		return true
	}
	h, _, err := net.SplitHostPort(host)
	if err != nil {
		log.WithError(err).WithContext(ctx).Debugf("failed to extract host from hostport: '%s'", host)
		h = host
	}
	s, _, err := net.SplitHostPort(sni)
	if err != nil {
		log.WithError(err).WithContext(ctx).Debugf("failed to extract sni from hostport: '%s'", sni)
		s = sni
	}
	span.SetAttributes(
		attribute.String("host_normalised", h),
		attribute.String("sni_normalised", s),
	)
	return h == s
}

func (router *CapRouter) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ctx, span := otel.Tracer(tracing.DefaultServiceName).Start(r.Context(), "router_cap_serveHTTP")
	defer span.End()
	fields := log.Fields{}
	scheme := SchemeHTTP
	// check the tls context
	if r.TLS != nil {
		scheme = SchemeHTTPS
		// if the browser is trying to reuse a connection
		// we need to stop it (see #24)
		if !router.allowSNISmuggling && !router.hostMatchesSNI(ctx, r.Host, r.TLS.ServerName) {
			log.WithFields(fields).WithContext(ctx).Warningf("detected SNI/VirtualHost mismatch (expected: %s, got: %s)", r.Host, r.TLS.ServerName)
			http.Error(w, "SNI and VirtualHost must match", http.StatusMisdirectedRequest)
			span.AddEvent("host and sni mismatch")
			router.proxyMetrics.sniMismatch.WithLabelValues(r.TLS.ServerName, r.Host).Inc()
			return
		}
		// if we've been given a client certificate
		// pass the subject and issuer upstream
		log.WithContext(ctx).WithFields(fields).Debugf("client presented %d certificate(s)", len(r.TLS.PeerCertificates))
		span.SetAttributes(attribute.Int("peer_certificates", len(r.TLS.PeerCertificates)))
		if len(r.TLS.PeerCertificates) > 0 {
			sub := r.TLS.PeerCertificates[0].Subject.ToRDNSequence().String()
			iss := r.TLS.PeerCertificates[0].Issuer.ToRDNSequence().String()
			r.Header.Set(HeaderSubject, sub)
			r.Header.Set(HeaderIssuer, iss)
			fields["sub"] = sub
			fields["iss"] = iss
			log.WithContext(ctx).WithFields(fields).Debug("detected identity in TLS request")
			// todo determine whether the cert is always valid at this point
			r.Header.Set(HeaderVerify, "SUCCESS")
			span.SetAttributes(attribute.String("sub", sub), attribute.String("iss", iss))
		}
	}
	span.SetAttributes(attribute.String("scheme", scheme))
	// we MUST use the host header as HTTP/2 allows
	// browsers to re-use tcp connections to the same origin
	//
	// https://bugzilla.mozilla.org/show_bug.cgi?id=1222136#c1
	target := r.Host
	if target == "" {
		log.WithContext(ctx).WithFields(fields).Warning("rejecting request due to missing Host header")
		http.Error(w, "Missing Host header", http.StatusBadRequest)
		span.AddEvent("missing host")
		return
	}
	span.SetAttributes(attribute.String("host", target))
	log.WithContext(ctx).WithFields(fields).Infof("routing '%s'", target)
	svc, ing, err := router.resolver.GetAddress(ctx, target, r.URL.Path)
	if err != nil {
		log.WithError(err).WithContext(ctx).WithFields(fields).Error("failed to resolve upstream")
		http.NotFound(w, r)
		span.RecordError(err)
		return
	}
	options := DefaultOptions()
	if err := tags.Marshal(options, ing.ObjectMeta.Annotations); err != nil {
		log.WithError(err).WithContext(ctx).WithFields(fields).Error("failed to marshal annotations")
		span.RecordError(err)
	}
	// convert the response into a usable url
	addr := router.getRoutableAddr(ctx, svc.String(router.clusterDomain), options)
	log.WithContext(ctx).WithFields(fields).Debugf("resolved upstream URI: '%s'", addr)
	uri, err := url.ParseRequestURI(addr)
	if err != nil {
		log.WithError(err).WithContext(ctx).WithFields(fields).Errorf("failed to parse upstream URI: '%s'", addr)
		http.Error(w, "failed to parse upstream URI", http.StatusInternalServerError)
		span.RecordError(err)
		return
	}
	log.WithContext(ctx).Infof("%s %s %s [%s]", r.Method, r.URL.Path, r.UserAgent(), svc.String(router.clusterDomain))
	log.WithFields(log.Fields{
		"method":        r.Method,
		"host":          r.Host,
		"scheme":        scheme,
		"remoteAddr":    r.RemoteAddr,
		"proto":         r.Proto,
		"contentLength": r.ContentLength,
	}).WithContext(ctx).Debug("proxying request")
	router.proxyMetrics.bytesSent.WithLabelValues("", r.Method, r.URL.Path, ing.Namespace, ing.Name, svc.GetName()).Observe(float64(r.ContentLength))
	router.proxyMetrics.requestLength.WithLabelValues("", r.Method, r.URL.Path, ing.Namespace, ing.Name, svc.GetName()).Observe(float64(r.ContentLength))
	span.SetAttributes(attribute.String("destination", uri.String()))
	// add proxy headers
	r.Header.Set(HeaderForwardedProto, scheme)
	proxy := httputil.NewSingleHostReverseProxy(uri)
	// use our configured transport
	proxy.Transport = router.getTransport(ctx, options)
	proxy.ModifyResponse = router.modifyResponse
	proxy.ErrorHandler = router.defaultErrorHandler
	proxy.ServeHTTP(w, r.Clone(context.WithValue(ctx, contextKeyInfo, contextInfo{
		Ingress:   ing.GetName(),
		Service:   svc.GetName(),
		Namespace: ing.GetNamespace(),
		Start:     time.Now(),
	})))
}

func (*CapRouter) getRoutableAddr(ctx context.Context, addr string, opts *Options) string {
	_, span := otel.Tracer(tracing.DefaultServiceName).Start(ctx, "router_cap_getRoutableAddr")
	defer span.End()
	var prefix string
	span.SetAttributes(attribute.String("proto_upstream", opts.UpstreamProtocol))
	switch strings.ToLower(opts.UpstreamProtocol) {
	default:
		log.Warningf("unknown or unsupported backend protocol: '%s'", opts.UpstreamProtocol)
		fallthrough
	case "":
		// default to http if nothing is specified
		fallthrough
	case SchemeH2C:
		fallthrough
	case SchemeHTTP:
		prefix = SchemeHTTP
	case SchemeHTTPS:
		prefix = SchemeHTTPS
	}
	span.SetAttributes(attribute.String("scheme_upstream", prefix))
	return fmt.Sprintf("%s://%s", prefix, addr)
}

// defaultErrorHandler is a minor extension of the default
// ErrorHandler. It performs the same, however it captures
// Prometheus metrics.
func (router *CapRouter) defaultErrorHandler(rw http.ResponseWriter, req *http.Request, err error) {
	ctx := req.Context()
	log.WithError(err).WithContext(ctx).Error("http: proxy error")
	// capture metrics
	status := "502"
	info, ok := ctx.Value(contextKeyInfo).(contextInfo)
	if ok {
		router.proxyMetrics.requests.WithLabelValues(info.Ingress, info.Namespace, status, info.Service, req.Proto).Inc()
		router.proxyMetrics.responseLength.WithLabelValues(status, req.Method, req.URL.Path, info.Namespace, info.Ingress, info.Service).Observe(0)
		router.proxyMetrics.responseTime.WithLabelValues(status, req.Method, req.URL.Path, info.Namespace, info.Ingress, info.Service).Observe(float64(time.Since(info.Start).Milliseconds()))
	}
	rw.WriteHeader(http.StatusBadGateway)
}

func (router *CapRouter) modifyResponse(r *http.Response) error {
	ctx, span := otel.Tracer(tracing.DefaultServiceName).Start(r.Request.Context(), "router_cap_modifyResponse")
	defer span.End()
	log.WithContext(ctx).Infof("%s %s %d %s", r.Request.Method, r.Request.URL.Path, r.StatusCode, r.Request.UserAgent())
	log.WithContext(ctx).WithFields(log.Fields{
		"status":        r.StatusCode,
		"proto":         r.Proto,
		"contentLength": r.ContentLength,
	}).Debug("proxying response")
	span.SetAttributes(
		attribute.Int("response_status", r.StatusCode),
		attribute.String("response_proto", r.Proto),
		attribute.Int64("response_length", r.ContentLength),
	)
	// collect request metrics
	status := strconv.Itoa(r.StatusCode)
	info, ok := ctx.Value(contextKeyInfo).(contextInfo)
	if ok {
		router.proxyMetrics.requests.WithLabelValues(info.Ingress, info.Namespace, status, info.Service, r.Request.Proto).Inc()
		router.proxyMetrics.responseLength.WithLabelValues(status, r.Request.Method, r.Request.URL.Path, info.Namespace, info.Ingress, info.Service).Observe(float64(r.ContentLength))
		router.proxyMetrics.responseTime.WithLabelValues(status, r.Request.Method, r.Request.URL.Path, info.Namespace, info.Ingress, info.Service).Observe(float64(time.Since(info.Start).Milliseconds()))
	}
	// inject hsts headers
	if router.hstsConfig.Enabled {
		span.AddEvent("hsts", trace.WithAttributes(attribute.String("hsts_header", router.hstsConfig.String())))
		r.Header.Set(HeaderSTS, router.hstsConfig.String())
	}
	// configure Alt-Svc headers if needed
	if router.altHeaderConfigurer != nil {
		if err := router.altHeaderConfigurer(r.Header); err != nil {
			log.WithError(err).WithContext(ctx).Error("failed to apply Alt-Svc headers")
		}
	}
	return nil
}

// getTransport creates a new http.Transport configured using our
// Options set.
func (router *CapRouter) getTransport(ctx context.Context, opts *Options) http.RoundTripper {
	_, span := otel.Tracer(tracing.DefaultServiceName).Start(ctx, "router_cap_getTransport")
	defer span.End()
	tlsConfig := &tls.Config{
		MinVersion:               tls.VersionTLS12,
		PreferServerCipherSuites: true,
		CipherSuites: []uint16{
			tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
			tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
			tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305,
			tls.TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,
		},
	}
	span.SetAttributes(attribute.String("scheme_upstream", opts.UpstreamProtocol))
	var t http.RoundTripper
	switch strings.ToLower(opts.UpstreamProtocol) {
	case SchemeH2C:
		span.SetAttributes(attribute.Bool("transport_h2c", true))
		t = &http2.Transport{
			TLSClientConfig: tlsConfig,
			AllowHTTP:       true,
			DialTLS: func(network, addr string, cfg *tls.Config) (net.Conn, error) {
				return net.Dial(network, addr)
			},
		}
	default:
		span.SetAttributes(attribute.Bool("transport_h2c", false))
		t = &http.Transport{
			// allow configurable connections, so we don't failed to
			// proxy requests when under high load
			// https://github.com/knative/serving/issues/2086#issuecomment-424605924
			MaxConnsPerHost:       router.transportConfig.MaxConnsPerHost,
			MaxIdleConns:          router.transportConfig.MaxIdleConns,
			MaxIdleConnsPerHost:   router.transportConfig.MaxIdleConnsPerHost,
			ReadBufferSize:        opts.UpstreamReadBufferSize,
			ResponseHeaderTimeout: time.Duration(opts.UpstreamTimeout) * time.Second,
			TLSHandshakeTimeout:   time.Duration(opts.UpstreamTLSTimeout) * time.Second,
			TLSClientConfig:       tlsConfig,
			ForceAttemptHTTP2:     true,
			// this was removed to provide consistent behaviour with h2c
			// and because it doesn't make sense for a reverse proxy to
			// respect proxy settings
			//Proxy: http.ProxyFromEnvironment,
		}
	}
	// since otelhttp doesn't support protocol switching
	// we need to use a forked version until it does
	// (see #28)
	return otelhttp.NewTransport(t)
}
