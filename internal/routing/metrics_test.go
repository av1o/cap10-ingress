package routing

import "github.com/prometheus/client_golang/prometheus"

// interface guard
var _ prometheus.Collector = NewProxyCollector("", "", "")
