package routing

import (
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/av1o/cap10-ingress/internal/metrics"
)

type ProxyCollector struct {
	prometheus.Collector

	requestTime   *prometheus.HistogramVec
	requestLength *prometheus.HistogramVec

	responseTime   *prometheus.HistogramVec
	responseLength *prometheus.HistogramVec

	upstreamLatency *prometheus.SummaryVec

	bytesSent *prometheus.HistogramVec

	requests    *prometheus.CounterVec
	sniMismatch *prometheus.CounterVec
}

var (
	requestTags = []string{
		"status",
		"method",
		"path",
		"namespace",
		"ingress",
		"service",
	}
)

func NewProxyCollector(pod, namespace, class string) *ProxyCollector {
	defaultLabels := prometheus.Labels{
		"controller_namespace": namespace,
		"controller_class":     class,
		"controller_pod":       pod,
	}
	return &ProxyCollector{
		requestTime: prometheus.NewHistogramVec(prometheus.HistogramOpts{
			Name:        "request_duration_seconds",
			Help:        "Request processing time in milliseconds",
			Namespace:   metrics.PrometheusNamespace,
			ConstLabels: defaultLabels,
			Buckets:     metrics.TimeBuckets,
		}, requestTags),
		requestLength: prometheus.NewHistogramVec(prometheus.HistogramOpts{
			Name:        "request_size",
			Help:        "Request length",
			Namespace:   metrics.PrometheusNamespace,
			ConstLabels: defaultLabels,
			Buckets:     metrics.LengthBuckets,
		}, requestTags),
		responseTime: prometheus.NewHistogramVec(prometheus.HistogramOpts{
			Name:        "response_duration_seconds",
			Help:        "Time spent receiving the response from the upstream server",
			Namespace:   metrics.PrometheusNamespace,
			ConstLabels: defaultLabels,
			Buckets:     metrics.TimeBuckets,
		}, requestTags),
		responseLength: prometheus.NewHistogramVec(prometheus.HistogramOpts{
			Name:        "response_size",
			Help:        "Response length",
			Namespace:   metrics.PrometheusNamespace,
			ConstLabels: defaultLabels,
			Buckets:     metrics.LengthBuckets,
		}, requestTags),
		upstreamLatency: prometheus.NewSummaryVec(prometheus.SummaryOpts{
			Name:        "ingress_upstream_latency_seconds",
			Help:        "Uptsream service latency per Ingress",
			Namespace:   metrics.PrometheusNamespace,
			ConstLabels: defaultLabels,
			Objectives:  metrics.DefaultObjectives,
		}, []string{"ingress", "namespace", "service"}),
		bytesSent: prometheus.NewHistogramVec(prometheus.HistogramOpts{
			Name:        "response_duration_seconds",
			Help:        "Time spent receiving the response from the upstream server",
			Namespace:   metrics.PrometheusNamespace,
			ConstLabels: defaultLabels,
			Buckets:     metrics.SizeBuckets,
		}, requestTags),
		requests: prometheus.NewCounterVec(prometheus.CounterOpts{
			Name:        "requests",
			Help:        "Total number of client requests",
			Namespace:   metrics.PrometheusNamespace,
			ConstLabels: defaultLabels,
		}, []string{"ingress", "namespace", "status", "service", "proto"}),
		sniMismatch: prometheus.NewCounterVec(prometheus.CounterOpts{
			Name:        "sni_mismatch",
			Help:        "Total number of requested issues where the SNI did not match the VirtualHost",
			Namespace:   metrics.PrometheusNamespace,
			ConstLabels: defaultLabels,
		}, []string{"sni", "host"}),
	}
}

func (c ProxyCollector) Describe(ch chan<- *prometheus.Desc) {
	c.requestTime.Describe(ch)
	c.requestLength.Describe(ch)

	c.responseTime.Describe(ch)
	c.responseLength.Describe(ch)

	c.upstreamLatency.Describe(ch)

	c.bytesSent.Describe(ch)

	c.requests.Describe(ch)
	c.sniMismatch.Describe(ch)
}

func (c ProxyCollector) Collect(ch chan<- prometheus.Metric) {
	c.requestTime.Collect(ch)
	c.requestLength.Collect(ch)

	c.responseTime.Collect(ch)
	c.responseLength.Collect(ch)

	c.upstreamLatency.Collect(ch)

	c.bytesSent.Collect(ch)

	c.requests.Collect(ch)
	c.sniMismatch.Collect(ch)
}
