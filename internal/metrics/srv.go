package metrics

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/collectors"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	log "github.com/sirupsen/logrus"
	"net/http"
	"os"
)

type Collector struct {
	port int

	registry *prometheus.Registry
}

func NewCollector(port int, prom ...prometheus.Collector) *Collector {
	reg := prometheus.NewRegistry()
	// register default collectors
	reg.MustRegister(collectors.NewGoCollector())
	reg.MustRegister(collectors.NewBuildInfoCollector())
	reg.MustRegister(collectors.NewProcessCollector(collectors.ProcessCollectorOpts{
		PidFn: func() (int, error) {
			return os.Getpid(), nil
		},
		ReportErrors: true,
	}))
	// register custom collectors
	for _, c := range prom {
		reg.MustRegister(c)
	}
	return &Collector{
		port:     port,
		registry: reg,
	}
}

func (s *Collector) Serve() {
	addr := fmt.Sprintf(":%d", s.port)
	log.WithField("addr", addr).Info("starting metrics server")

	router := mux.NewRouter()
	router.Handle("/metrics", promhttp.HandlerFor(s.registry, promhttp.HandlerOpts{
		Registry:          s.registry,
		EnableOpenMetrics: true,
	}))
	// start the http server
	log.Fatal(http.ListenAndServe(addr, router))
}
