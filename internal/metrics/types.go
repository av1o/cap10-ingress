package metrics

import "github.com/prometheus/client_golang/prometheus"

const PrometheusNamespace = "cap10_ingress_controller"

var DefaultObjectives = map[float64]float64{0.5: 0.05, 0.9: 0.01, 0.99: 0.001}

var (
	TimeBuckets   = prometheus.DefBuckets
	LengthBuckets = prometheus.LinearBuckets(10, 10, 10)
	SizeBuckets   = prometheus.ExponentialBuckets(10, 10, 7)
)
