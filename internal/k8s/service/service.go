package service

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/av1o/cap10-ingress/pkg/tracing"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

type Reader struct {
	client kubernetes.Interface
}

func NewReader(client kubernetes.Interface) *Reader {
	return &Reader{
		client: client,
	}
}

func (r *Reader) GetPortByName(ctx context.Context, name, namespace, portName string) (int32, error) {
	ctx, span := otel.Tracer(tracing.DefaultServiceName).Start(ctx, "reader_port_getPortByName", trace.WithAttributes(
		attribute.String("name", name),
		attribute.String("namespace", namespace),
		attribute.String("port_name", portName),
	))
	defer span.End()
	fields := log.Fields{
		"name":      name,
		"namespace": namespace,
		"port":      portName,
	}
	log.WithContext(ctx).WithFields(fields).Debug("attempting to fetch service")
	service, err := r.client.CoreV1().Services(namespace).Get(ctx, name, metav1.GetOptions{})
	if err != nil {
		span.RecordError(err)
		log.WithError(err).WithContext(ctx).WithFields(fields).Error("failed to retrieve service")
		return 0, err
	}
	// find the port of interest
	log.WithContext(ctx).WithFields(fields).Debugf("successfully fetched service with %d ports", len(service.Spec.Ports))
	for _, port := range service.Spec.Ports {
		if port.Name == portName {
			span.AddEvent("found port number", trace.WithAttributes(attribute.Int("port_number", int(port.Port))))
			return port.Port, nil
		}
	}
	// todo should we try to find the next-best port with no exact match?
	// if we don't this will cause a routing failure
	log.WithContext(ctx).WithFields(fields).Warning("failed to find matching port")
	span.RecordError(ErrPortNotFound)
	return 0, ErrPortNotFound
}
