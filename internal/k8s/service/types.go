package service

import (
	"context"
	"errors"
)

var (
	// ErrPortNotFound is thrown when requesting a port that
	// is not known to the service.
	ErrPortNotFound = errors.New("failed to find a matching port")
)

type PortNameResolver interface {
	GetPortByName(ctx context.Context, name, namespace, portName string) (int32, error)
}
