package service

import (
	"context"
	"github.com/stretchr/testify/assert"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes/fake"
	"testing"
)

const (
	NameValid   = "service-valid"
	NameInvalid = "service-invalid"
)

// interface guard
var _ PortNameResolver = &Reader{}

func TestNewReader(t *testing.T) {
	assert.NotNil(t, NewReader(fake.NewSimpleClientset()))
}

func TestReader_GetPortByName(t *testing.T) {
	client := fake.NewSimpleClientset(&v1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:      NameValid,
			Namespace: "default",
		},
		Spec: v1.ServiceSpec{
			Ports: []v1.ServicePort{
				{
					Name: "web",
					Port: 8080,
				},
				{
					Name: "rpc",
					Port: 8081,
				},
			},
		},
	}, &v1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:      NameInvalid,
			Namespace: "default",
		},
		Spec: v1.ServiceSpec{
			Ports: []v1.ServicePort{},
		},
	})

	var cases = []struct {
		name     string
		svcName  string
		portName string
		expected int32
	}{
		{
			"lookup works",
			NameValid,
			"web",
			8080,
		},
		{
			"lookup works for multi-port",
			NameValid,
			"rpc",
			8081,
		},
		{
			"lookup failed when port not found",
			NameInvalid,
			"web",
			0,
		},
		{
			"lookup failed when service not found",
			"not-real",
			"web",
			0,
		},
	}

	reader := NewReader(client)

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			port, err := reader.GetPortByName(context.TODO(), tt.svcName, "default", tt.portName)
			if tt.expected == 0 {
				assert.Error(t, err)
				assert.Zero(t, port)
			} else {
				assert.NoError(t, err)
				assert.EqualValues(t, tt.expected, port)
			}
		})
	}
}
