package config

import (
	"context"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/av1o/cap10-ingress/internal/config"
	"gitlab.com/av1o/cap10-ingress/internal/testutils"
	v1 "k8s.io/api/networking/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"testing"
)

// interface guard
var _ ResolverInterface = NewResolver(&testutils.PortResolver{})

func TestResolver_GetAddress(t *testing.T) {
	ctx := context.TODO()
	pathType := v1.PathTypeImplementationSpecific
	pathTypeExact := v1.PathTypeExact
	r := NewResolver(&testutils.PortResolver{})
	sd, _, err := r.GetAddress(context.TODO(), "", "")
	assert.Nil(t, sd)
	assert.ErrorIs(t, err, ErrNoUpstream)

	r.Add(ctx, &v1.Ingress{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "gitlab-webservice-default",
			Namespace: "gitlab",
		},
		Spec: v1.IngressSpec{
			Rules: []v1.IngressRule{
				{
					Host: "gitlab.example.org",
					IngressRuleValue: v1.IngressRuleValue{
						HTTP: &v1.HTTPIngressRuleValue{
							Paths: []v1.HTTPIngressPath{
								{
									Path:     "/",
									PathType: &pathType,
									Backend: v1.IngressBackend{
										Service: &v1.IngressServiceBackend{
											Name: "gitlab-webservice-default",
											Port: v1.ServiceBackendPort{
												Number: 8080,
											},
										},
									},
								},
							},
						},
					},
				},
				{
					Host: "registry.example.org",
					IngressRuleValue: v1.IngressRuleValue{
						HTTP: &v1.HTTPIngressRuleValue{
							Paths: []v1.HTTPIngressPath{
								{
									Path:     "/v2",
									PathType: &pathType,
									Backend: v1.IngressBackend{
										Service: &v1.IngressServiceBackend{
											Name: "gitlab-registry",
											Port: v1.ServiceBackendPort{
												Number: 8080,
											},
										},
									},
								},
							},
						},
					},
				},
				{
					Host: "gitlab.example.org",
					IngressRuleValue: v1.IngressRuleValue{
						HTTP: &v1.HTTPIngressRuleValue{
							Paths: []v1.HTTPIngressPath{
								{
									Path:     "/api",
									PathType: &pathTypeExact,
									Backend: v1.IngressBackend{
										Service: &v1.IngressServiceBackend{
											Name: "gitlab-foobar",
											Port: v1.ServiceBackendPort{
												Name: "web",
											},
										},
									},
								},
							},
						},
					},
				},
			},
		},
	})

	t.Run("prefix can be resolved", func(t *testing.T) {
		sd, _, err = r.GetAddress(context.TODO(), "gitlab.example.org", "/")
		assert.NoError(t, err)
		assert.EqualValues(t, "gitlab-webservice-default.gitlab.svc.cluster.local:8080", sd.String(config.DefaultClusterDomain))
	})
	t.Run("exact path can be resolved", func(t *testing.T) {
		sd, _, err = r.GetAddress(context.TODO(), "gitlab.example.org", "/api")
		assert.NoError(t, err)
		assert.EqualValues(t, "gitlab-foobar.gitlab.svc.cluster.local:8080", sd.String(config.DefaultClusterDomain))
	})
	t.Run("exact path is exact", func(t *testing.T) {
		sd, _, err = r.GetAddress(context.TODO(), "gitlab.example.org", "/api/foobar")
		assert.NoError(t, err)
		assert.EqualValues(t, "gitlab-webservice-default.gitlab.svc.cluster.local:8080", sd.String(config.DefaultClusterDomain))
	})
	t.Run("incoming port is ignored", func(t *testing.T) {
		sd, _, err = r.GetAddress(context.TODO(), "gitlab.example.org:443", "/api/foobar")
		assert.NoError(t, err)
		assert.EqualValues(t, "gitlab-webservice-default.gitlab.svc.cluster.local:8080", sd.String(config.DefaultClusterDomain))
	})
}

func TestResolver_IsPathType(t *testing.T) {
	ipl := v1.PathTypeImplementationSpecific
	exact := v1.PathTypeExact

	r := NewResolver(&testutils.PortResolver{})
	assert.False(t, r.isPathType(nil, false))
	assert.True(t, r.isPathType(nil, true))
	assert.True(t, r.isPathType(&ipl, false, v1.PathTypeImplementationSpecific, v1.PathTypePrefix))
	assert.False(t, r.isPathType(&exact, false, v1.PathTypeImplementationSpecific, v1.PathTypePrefix))
}

func TestResolver_GetAddress_NoPath(t *testing.T) {
	ctx := context.TODO()
	r := NewResolver(&testutils.PortResolver{})
	pathType := v1.PathTypeImplementationSpecific
	r.Add(ctx, &v1.Ingress{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "ingress-no-path",
			Namespace: "default",
		},
		Spec: v1.IngressSpec{
			Rules: []v1.IngressRule{
				{
					Host: "foo.bar.com",
					IngressRuleValue: v1.IngressRuleValue{
						HTTP: &v1.HTTPIngressRuleValue{
							Paths: []v1.HTTPIngressPath{
								{
									PathType: &pathType,
									Backend: v1.IngressBackend{
										Service: &v1.IngressServiceBackend{
											Name: "service1",
											Port: v1.ServiceBackendPort{
												Number: 80,
											},
										},
									},
								},
							},
						},
					},
				},
			},
		},
	})
	log.SetLevel(log.DebugLevel)
	t.Run("missing path resolves", func(t *testing.T) {
		sd, _, err := r.GetAddress(context.TODO(), "foo.bar.com", "/foo/bar")
		assert.NoError(t, err)
		assert.EqualValues(t, "service1.default.svc.cluster.local:80", sd.String(config.DefaultClusterDomain))
	})
}

func TestResolver_GetAddress_Wildcard(t *testing.T) {
	ctx := context.TODO()
	r := NewResolver(&testutils.PortResolver{})
	// ingress borrowed from https://kubernetes.io/docs/concepts/services-networking/ingress/#hostname-wildcards
	r.Add(ctx, &v1.Ingress{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "ingress-wildcard-host",
			Namespace: "default",
		},
		Spec: v1.IngressSpec{
			Rules: []v1.IngressRule{
				{
					Host: "foo.bar.com",
					IngressRuleValue: v1.IngressRuleValue{
						HTTP: &v1.HTTPIngressRuleValue{
							Paths: []v1.HTTPIngressPath{
								{
									Path: "/bar",
									Backend: v1.IngressBackend{
										Service: &v1.IngressServiceBackend{
											Name: "service1",
											Port: v1.ServiceBackendPort{
												Number: 80,
											},
										},
									},
								},
							},
						},
					},
				},
				{
					Host: "*.foo.com",
					IngressRuleValue: v1.IngressRuleValue{
						HTTP: &v1.HTTPIngressRuleValue{
							Paths: []v1.HTTPIngressPath{
								{
									Path: "/foo",
									Backend: v1.IngressBackend{
										Service: &v1.IngressServiceBackend{
											Name: "service2",
											Port: v1.ServiceBackendPort{
												Number: 80,
											},
										},
									},
								},
							},
						},
					},
				},
			},
		},
	})
	log.SetLevel(log.DebugLevel)
	t.Run("wildcard hostname resolves", func(t *testing.T) {
		sd, _, err := r.GetAddress(context.TODO(), "bar.foo.com", "/foo/bar")
		assert.NoError(t, err)
		assert.EqualValues(t, "service2.default.svc.cluster.local:80", sd.String(config.DefaultClusterDomain))
	})
	t.Run("standard hostname resolves", func(t *testing.T) {
		sd, _, err := r.GetAddress(context.TODO(), "foo.bar.com", "/bar/foo")
		assert.NoError(t, err)
		assert.NotNil(t, sd)
		assert.EqualValues(t, "service1.default.svc.cluster.local:80", sd.String(config.DefaultClusterDomain))
	})
}
