package config

import (
	"context"
	"errors"
	"fmt"
	"gitlab.com/av1o/cap10-ingress/internal/config"
	v1 "k8s.io/api/networking/v1"
)

var (
	// ErrNoUpstream is thrown when we couldn't find a suitable Ingress
	// to receive traffic
	ErrNoUpstream = errors.New("failed to find matching upstream")
)

type ServiceDescriptor struct {
	name      string
	namespace string
	port      int32
}

func NewServiceDescriptor(name string, namespace string, port int32) *ServiceDescriptor {
	return &ServiceDescriptor{
		name:      name,
		namespace: namespace,
		port:      port,
	}
}

func (d *ServiceDescriptor) String(clusterDomain string) string {
	if clusterDomain == "" {
		clusterDomain = config.DefaultClusterDomain
	}
	return fmt.Sprintf("%s.%s.svc.%s:%d", d.name, d.namespace, clusterDomain, d.port)
}

func (d *ServiceDescriptor) GetName() string {
	return d.name
}

type ResolverInterface interface {
	GetAddress(ctx context.Context, host, path string) (*ServiceDescriptor, *v1.Ingress, error)
	GetSNI(ctx context.Context, sni string) (*ServiceDescriptor, *v1.Ingress, error)
}
