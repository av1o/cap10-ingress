package config

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/av1o/cap10-ingress/internal/k8s/service"
	"gitlab.com/av1o/cap10-ingress/pkg/tracing"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
	v1 "k8s.io/api/networking/v1"
	"net"
	"strings"
)

type Resolver struct {
	ingresses    []*v1.Ingress
	portResolver service.PortNameResolver
}

// NewResolver creates a Resolver.
func NewResolver(portResolver service.PortNameResolver) *Resolver {
	return &Resolver{
		ingresses:    []*v1.Ingress{},
		portResolver: portResolver,
	}
}

func (r *Resolver) isPathType(pathType *v1.PathType, allowNil bool, matches ...v1.PathType) bool {
	if pathType == nil && !allowNil {
		return false
	} else if pathType == nil {
		return true
	}
	pt := *pathType
	for _, m := range matches {
		if pt == m {
			return true
		}
	}
	return false
}

// getPort gets the numerical port number of a backend.
//
// If the number is given in the backend that is returned,
// otherwise the bound service is looked up and the number is fetched from that
func (r *Resolver) getPort(ctx context.Context, backend *v1.IngressServiceBackend, namespace string) int32 {
	ctx, span := otel.Tracer(tracing.DefaultServiceName).Start(ctx, "resolver_getPort", trace.WithAttributes(
		attribute.String("namespace", namespace),
		attribute.Int("port_number", int(backend.Port.Number)),
	))
	defer span.End()
	if backend.Port.Number > 0 {
		span.AddEvent("found port number", trace.WithAttributes(attribute.Int("port_number", int(backend.Port.Number))))
		return backend.Port.Number
	}
	log.WithContext(ctx).Debug("failed to fetch port number from Ingress, doing slow Service lookup")
	// todo port-name mappings should be cached
	port, err := r.portResolver.GetPortByName(ctx, backend.Name, namespace, backend.Port.Name)
	if err != nil {
		span.RecordError(err)
		log.WithError(err).Error("failed to fetch port by name - this may cause routing issues")
		return 0
	}
	return port
}

func (r *Resolver) matchSNI(ctx context.Context, sni string, ingresses []*v1.Ingress) (*ServiceDescriptor, *v1.Ingress) {
	ctx, span := otel.Tracer(tracing.DefaultServiceName).Start(ctx, "resolver_matchSNI", trace.WithAttributes(
		attribute.String("sni", sni),
		attribute.Int("ingresses", len(ingresses)),
	))
	defer span.End()
	var matchIng *v1.Ingress
	var match *ServiceDescriptor
	for _, i := range ingresses {
		// ensure that the appropriate annotation is set
		if i.Annotations["nginx.ingress.kubernetes.io/ssl-passthrough"] != "true" && i.Annotations["cap10.ingress.dcas.dev/ssl-passthrough"] != "true" {
			continue
		}
		for _, rule := range i.Spec.Rules {
			h := strings.TrimPrefix(rule.Host, "*")
			switch {
			case h != rule.Host && strings.HasSuffix(sni, h):
				span.AddEvent("SNI matched wildcard", trace.WithAttributes(attribute.String("name", i.GetName())))
				log.WithContext(ctx).Debugf("SNI '%s' matched wildcard '%s' [%s/%s]", sni, rule.Host, i.GetNamespace(), i.GetName())
			case rule.Host == sni:
				span.AddEvent("SNI matched exactly", trace.WithAttributes(attribute.String("name", i.GetName())))
				log.WithContext(ctx).Debugf("SNI '%s' matched exactly [%s/%s]", sni, i.GetNamespace(), i.GetName())
			default:
				continue
			}
			if len(rule.HTTP.Paths) == 0 {
				continue
			}
			// use the service from the first path.
			// Additional paths are useless since we
			// can't see anything other than the SNI.
			match = &ServiceDescriptor{
				name:      rule.HTTP.Paths[0].Backend.Service.Name,
				namespace: i.GetNamespace(),
				port:      r.getPort(ctx, rule.HTTP.Paths[0].Backend.Service, i.GetNamespace()),
			}
			matchIng = i
		}
	}
	return match, matchIng
}

func (r *Resolver) match(ctx context.Context, host, path string, ingresses []*v1.Ingress) (*ServiceDescriptor, *v1.Ingress) {
	ctx, span := otel.Tracer(tracing.DefaultServiceName).Start(ctx, "resolver_match", trace.WithAttributes(
		attribute.String("host", host),
		attribute.String("path", path),
		attribute.Int("ingresses", len(ingresses)),
	))
	defer span.End()
	// check if there's an exact match
	var matchIng *v1.Ingress
	var match *ServiceDescriptor
	var longestPath string
	for _, i := range ingresses {
		// ensure that we skip SNI ingresses
		if i.Annotations["nginx.ingress.kubernetes.io/ssl-passthrough"] == "true" {
			continue
		}
		if i.Annotations["cap10.ingress.dcas.dev/ssl-passthrough"] == "true" {
			continue
		}
		for _, rule := range i.Spec.Rules {
			// check that the host matches
			h := strings.TrimPrefix(rule.Host, "*")
			switch {
			case h != rule.Host && strings.HasSuffix(host, h):
				span.AddEvent("hostname matched wildcard", trace.WithAttributes(attribute.String("name", i.GetName())))
				log.WithContext(ctx).Debugf("hostname '%s' matched wildcard '%s' [%s/%s]", host, rule.Host, i.GetNamespace(), i.GetName())
			case rule.Host == host:
				span.AddEvent("hostname matched exactly", trace.WithAttributes(attribute.String("name", i.GetName())))
				log.WithContext(ctx).Debugf("hostname '%s' matched exactly [%s/%s]", host, i.GetNamespace(), i.GetName())
			default:
				continue
			}
			// check that the path matches
			log.WithContext(ctx).Debugf("checking %d path(s) for match against '%s'", len(rule.HTTP.Paths), path)
			for _, p := range rule.HTTP.Paths {
				rulePath := p.Path
				// if there was no path, create a fake one
				if rulePath == "" {
					rulePath = "/"
				}
				// if pathtype is exact or impl and the path matches
				// return that straight away
				if r.isPathType(p.PathType, false, v1.PathTypeExact, v1.PathTypeImplementationSpecific) && rulePath == path {
					span.AddEvent("path matched exactly", trace.WithAttributes(attribute.String("name", i.GetName())))
					log.WithContext(ctx).Debugf("found exact match with %s/%s", i.GetNamespace(), i.GetName())
					return &ServiceDescriptor{
						name:      p.Backend.Service.Name,
						namespace: i.GetNamespace(),
						port:      r.getPort(ctx, p.Backend.Service, i.GetNamespace()),
					}, i
				}
				// if the pathtype is prefix, impl or not specified
				// return the longest path match
				if r.isPathType(p.PathType, true, v1.PathTypePrefix, v1.PathTypeImplementationSpecific) && strings.HasPrefix(path, rulePath) && len(rulePath) > len(longestPath) {
					span.AddEvent("path matched prefix", trace.WithAttributes(attribute.String("name", i.GetName())))
					log.WithContext(ctx).Debugf("found potential prefix match (%s) with %s/%s", rulePath, i.GetNamespace(), i.GetName())
					longestPath = rulePath
					match = &ServiceDescriptor{
						name:      p.Backend.Service.Name,
						namespace: i.GetNamespace(),
						port:      r.getPort(ctx, p.Backend.Service, i.GetNamespace()),
					}
					matchIng = i
				}
			}
		}
	}
	// return the longest prefix match
	return match, matchIng
}

func (r *Resolver) Add(ctx context.Context, obj *v1.Ingress) {
	ctx, span := otel.Tracer(tracing.DefaultServiceName).Start(ctx, "resolver_add", trace.WithAttributes(
		attribute.String("name", obj.GetName()),
		attribute.String("namespace", obj.GetNamespace()),
	))
	defer span.End()
	fields := log.Fields{
		"name":      obj.GetName(),
		"namespace": obj.GetNamespace(),
	}
	log.WithFields(fields).WithContext(ctx).Infof("upserting ingress")
	var ing *v1.Ingress
	for i := range r.ingresses {
		ing = r.ingresses[i]
		// if there's a match, update that
		if ing.GetUID() == obj.GetUID() {
			log.WithFields(fields).WithContext(ctx).Debugf("during upsert, we decided to update (gen %d vs gen %d)", ing.GetGeneration(), obj.GetGeneration())
			r.ingresses[i] = obj
			return
		}
	}
	// otherwise, append it
	r.ingresses = append(r.ingresses, obj)
	log.WithFields(fields).WithContext(ctx).Debug("during upsert, we decided to insert")
}

func (r *Resolver) Delete(ctx context.Context, obj *v1.Ingress) {
	ctx, span := otel.Tracer(tracing.DefaultServiceName).Start(ctx, "resolver_delete", trace.WithAttributes(
		attribute.String("name", obj.GetName()),
		attribute.String("namespace", obj.GetNamespace()),
	))
	defer span.End()
	fields := log.Fields{
		"name":      obj.GetName(),
		"namespace": obj.GetNamespace(),
	}
	log.WithFields(fields).WithContext(ctx).Infof("deleting ingress")
	var ing *v1.Ingress
	for i := range r.ingresses {
		ing = r.ingresses[i]
		// if there's a match, update that
		if ing.GetUID() == obj.GetUID() {
			r.ingresses[i] = obj
			log.WithFields(fields).WithContext(ctx).Debugf("deleting ingress: %s", ing.GetUID())
			return
		}
	}
}

func (r *Resolver) GetAddress(ctx context.Context, host, path string) (*ServiceDescriptor, *v1.Ingress, error) {
	ctx, span := otel.Tracer(tracing.DefaultServiceName).Start(ctx, "resolver_getAddress", trace.WithAttributes(
		attribute.String("host", host),
		attribute.String("path", path),
	))
	defer span.End()
	log.WithContext(ctx).Debugf("attempting to resolve address: %s%s", host, path)
	if strings.Contains(host, ":") {
		h, _, err := net.SplitHostPort(host)
		if err != nil {
			log.WithError(err).WithContext(ctx).Errorf("failed to parse host: '%s'", host)
			return nil, nil, err
		}
		log.WithContext(ctx).Debugf("extract host '%s' from host:port", h)
		host = h
	}
	match, ing := r.match(ctx, host, path, r.ingresses)
	if match != nil {
		log.WithContext(ctx).Info("successfully fetched upstream")
		return match, ing, nil
	}
	span.RecordError(ErrNoUpstream)
	return nil, nil, ErrNoUpstream
}

func (r *Resolver) GetSNI(ctx context.Context, sni string) (*ServiceDescriptor, *v1.Ingress, error) {
	ctx, span := otel.Tracer(tracing.DefaultServiceName).Start(ctx, "resolver_getSNI", trace.WithAttributes(
		attribute.String("sni", sni),
	))
	defer span.End()
	log.WithContext(ctx).Debugf("attempting to resolve SNI: %s", sni)
	if strings.Contains(sni, ":") {
		s, _, err := net.SplitHostPort(sni)
		if err != nil {
			log.WithError(err).WithContext(ctx).Errorf("failed to parse SNI: '%s'", sni)
			return nil, nil, err
		}
		log.WithContext(ctx).Debugf("extracted SNI '%s' from host:port", s)
		sni = s
	}
	match, ing := r.matchSNI(ctx, sni, r.ingresses)
	if match != nil {
		log.WithContext(ctx).Info("successfully fetched SNI upstream")
		return match, ing, nil
	}
	span.RecordError(ErrNoUpstream)
	return nil, nil, ErrNoUpstream
}
