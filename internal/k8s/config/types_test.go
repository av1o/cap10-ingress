package config

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/av1o/cap10-ingress/internal/config"
	"testing"
)

func TestServiceDescriptor_String(t *testing.T) {
	sd := &ServiceDescriptor{
		name:      "gitlab-webservice-default",
		namespace: "gitlab",
		port:      8080,
	}
	assert.EqualValues(t, "gitlab-webservice-default.gitlab.svc.cluster.local:8080", sd.String(config.DefaultClusterDomain))
	assert.EqualValues(t, "gitlab-webservice-default.gitlab.svc.cluster.local:8080", sd.String(""))
	assert.EqualValues(t, "gitlab-webservice-default.gitlab.svc.foo.bar:8080", sd.String("foo.bar"))
}
