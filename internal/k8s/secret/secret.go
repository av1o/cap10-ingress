package secret

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	log "github.com/sirupsen/logrus"
	"gitlab.com/av1o/cap10-ingress/pkg/tracing"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

type Reader struct {
	client kubernetes.Interface
}

func NewReader(client kubernetes.Interface) *Reader {
	return &Reader{
		client: client,
	}
}

func (r *Reader) ReadSecret(ctx context.Context, name, namespace string) (*tls.Certificate, error) {
	ctx, span := otel.Tracer(tracing.DefaultServiceName).Start(ctx, "reader_secret_readSecret", trace.WithAttributes(
		attribute.String("name", name),
		attribute.String("namespace", namespace),
	))
	defer span.End()
	fields := log.Fields{
		"name":      name,
		"namespace": namespace,
	}
	log.WithContext(ctx).WithFields(fields).Debug("attempting to fetch secret")
	secret, err := r.client.CoreV1().Secrets(namespace).Get(ctx, name, metav1.GetOptions{})
	if err != nil {
		span.RecordError(err)
		log.WithError(err).WithContext(ctx).WithFields(fields).Error("failed to retrieve secret")
		return nil, err
	}
	// check that the secret is actually a tls cert
	if secret.Type != v1.SecretTypeTLS {
		span.RecordError(ErrInvalidType)
		log.WithContext(ctx).WithFields(fields).Errorf("invalid secret type (expecting: %s, got: %s)", v1.SecretTypeTLS, secret.Type)
		return nil, ErrInvalidType
	}
	log.WithContext(ctx).WithFields(fields).Debug("successfully fetched secret, reading tls data")
	// extract the cert
	cert, ok := secret.Data[KeyCert]
	if !ok {
		span.RecordError(ErrMissingCert)
		log.WithContext(ctx).WithFields(fields).Error("failed to extract x509 keypair as the tls.crt field is empty")
		return nil, ErrMissingCert
	}
	// extract the key
	key, ok := secret.Data[KeyKey]
	if !ok {
		span.RecordError(ErrMissingKey)
		log.WithContext(ctx).WithFields(fields).Error("failed to extract x509 keypair as the tls.key field is empty")
		return nil, ErrMissingKey
	}
	log.WithContext(ctx).WithFields(fields).Debug("successfully read tls data from secret")
	keyPair, err := tls.X509KeyPair(cert, key)
	if err != nil {
		span.RecordError(err)
		log.WithError(err).WithContext(ctx).WithFields(fields).Error("failed to parse x509 keypair from given data")
		return nil, err
	}
	log.WithContext(ctx).WithFields(fields).Debug("successfully parsed x509 keypair")
	return &keyPair, nil
}

// ReadCA reads a certificate authority from a
// kubernetes secret.
//
// It expects the pem-encoded CA to be in the ca.crt
// key.
func (r *Reader) ReadCA(ctx context.Context, name, namespace string) (*x509.CertPool, error) {
	ctx, span := otel.Tracer(tracing.DefaultServiceName).Start(ctx, "reader_secret_readCA", trace.WithAttributes(
		attribute.String("name", name),
		attribute.String("namespace", namespace),
	))
	defer span.End()
	fields := log.Fields{
		"name":      name,
		"namespace": namespace,
	}
	log.WithContext(ctx).WithFields(fields).Debug("attempting to fetch secret")
	secret, err := r.client.CoreV1().Secrets(namespace).Get(ctx, name, metav1.GetOptions{})
	if err != nil {
		span.RecordError(err)
		log.WithError(err).WithContext(ctx).WithFields(fields).Error("failed to retrieve secret")
		return nil, err
	}
	log.WithContext(ctx).WithFields(fields).Debug("successfully fetched secret, reading tls data")
	// extract the cert
	cert, ok := secret.Data[KeyCA]
	if !ok {
		span.RecordError(ErrMissingCA)
		log.WithContext(ctx).WithFields(fields).Error("failed to extract x509 keypair as the ca.crt field is empty")
		return nil, ErrMissingCA
	}
	// create a new cert pool and append the CA
	pool := x509.NewCertPool()
	ok = pool.AppendCertsFromPEM(cert)
	if !ok {
		span.RecordError(ErrFailedToAppend)
		log.WithContext(ctx).WithFields(fields).Error("failed to append certificates to bundle - they may not be valid")
		return nil, ErrFailedToAppend
	}
	log.WithContext(ctx).WithFields(fields).Debugf("successfully appended %d certificates to bundle", len(pool.Subjects()))
	return pool, nil
}
