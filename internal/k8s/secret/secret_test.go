package secret

import (
	"context"
	_ "embed"
	"github.com/stretchr/testify/assert"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes/fake"
	"testing"
)

const (
	NameValid      = "valid-tls-secret"
	NameWrongType  = "valid-wrong-type"
	NameInvalid    = "invalid-tls-secret"
	NameInvalidKey = "invalid-key"
	NameBadData    = "invalid-bad-data"
)

//go:embed testdata/tls.crt
var rsaCertPEM []byte

//go:embed testdata/tls.key
var keyPEM []byte

func TestNewReader(t *testing.T) {
	assert.NotNil(t, NewReader(fake.NewSimpleClientset()))
}

func TestReader_ReadSecret(t *testing.T) {
	client := fake.NewSimpleClientset(&v1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      NameValid,
			Namespace: "default",
		},
		Data: map[string][]byte{
			KeyCert: rsaCertPEM,
			KeyKey:  keyPEM,
		},
		Type: v1.SecretTypeTLS,
	}, &v1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      NameWrongType,
			Namespace: "default",
		},
		Data: map[string][]byte{
			KeyCert: rsaCertPEM,
			KeyKey:  keyPEM,
		},
		Type: v1.SecretTypeOpaque,
	}, &v1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      NameInvalid,
			Namespace: "default",
		},
		Data: map[string][]byte{
			"key": []byte("value"),
		},
		Type: v1.SecretTypeTLS,
	}, &v1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      NameInvalidKey,
			Namespace: "default",
		},
		Data: map[string][]byte{
			KeyCert: rsaCertPEM,
		},
		Type: v1.SecretTypeTLS,
	}, &v1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      NameBadData,
			Namespace: "default",
		},
		Data: map[string][]byte{
			KeyCert: rsaCertPEM,
			KeyKey:  []byte("foobar"),
		},
		Type: v1.SecretTypeTLS,
	})

	var cases = []struct {
		name string
		err  bool
	}{
		{
			NameValid,
			false,
		},
		{
			NameInvalid,
			true,
		},
		{
			NameWrongType,
			true,
		},
		{
			"not-real",
			true,
		},
		{
			NameInvalidKey,
			true,
		},
		{
			NameBadData,
			true,
		},
	}

	r := NewReader(client)
	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			cert, err := r.ReadSecret(context.TODO(), tt.name, "default")
			if tt.err {
				assert.Nil(t, cert)
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
				assert.NotNil(t, cert)
			}
		})
	}
}

func TestReader_ReadCA(t *testing.T) {
	client := fake.NewSimpleClientset(&v1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      NameValid,
			Namespace: "default",
		},
		Data: map[string][]byte{
			KeyCA: rsaCertPEM,
		},
		Type: v1.SecretTypeTLS,
	}, &v1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      NameInvalid,
			Namespace: "default",
		},
		Data: map[string][]byte{
			KeyCA: keyPEM,
		},
		Type: v1.SecretTypeTLS,
	}, &v1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      NameBadData,
			Namespace: "default",
		},
		Data: map[string][]byte{
			KeyCert: rsaCertPEM,
		},
		Type: v1.SecretTypeTLS,
	})

	var cases = []struct {
		name string
		err  bool
	}{
		{
			NameValid,
			false,
		},
		{
			NameInvalid,
			true,
		},
		{
			NameWrongType,
			true,
		},
		{
			"not-real",
			true,
		},
		{
			NameBadData,
			true,
		},
	}

	r := NewReader(client)
	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			cert, err := r.ReadCA(context.TODO(), tt.name, "default")
			if tt.err {
				assert.Nil(t, cert)
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
				assert.NotNil(t, cert)
			}
		})
	}
}
