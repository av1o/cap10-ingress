package secret

import "errors"

const (
	KeyCert = "tls.crt"
	KeyKey  = "tls.key"
	KeyCA   = "ca.crt"
)

var (
	// ErrInvalidType is thrown when the Secret type is
	// not correct
	ErrInvalidType    = errors.New("invalid secret type")
	ErrMissingCert    = errors.New("secret missing tls.crt")
	ErrMissingKey     = errors.New("secret missing tls.key")
	ErrMissingCA      = errors.New("secret missing ca.crt")
	ErrFailedToAppend = errors.New("failed to append certificate bundle")
)
