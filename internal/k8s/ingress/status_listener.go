package ingress

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/av1o/cap10-ingress/pkg/tracing"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
	corev1 "k8s.io/api/core/v1"
	v1 "k8s.io/api/networking/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"strings"
)

type StatusListener struct {
	client kubernetes.Interface
}

func NewStatusListener(client kubernetes.Interface) *StatusListener {
	return &StatusListener{
		client: client,
	}
}

func (l *StatusListener) Add(ctx context.Context, ingress *v1.Ingress) {
	ctx, span := otel.Tracer(tracing.DefaultServiceName).Start(ctx, "listener_status_add", trace.WithAttributes(
		attribute.String("name", ingress.GetName()),
		attribute.String("namespace", ingress.GetNamespace()),
	))
	defer span.End()
	fields := log.Fields{"name": ingress.GetName()}
	log.WithContext(ctx).WithFields(fields).Debugf("attempting to update Ingress status")
	// collect a list of all the hosts
	hosts := make([]corev1.LoadBalancerIngress, len(ingress.Spec.Rules))
	for i := range ingress.Spec.Rules {
		hosts[i] = corev1.LoadBalancerIngress{
			Hostname: strings.ReplaceAll(ingress.Spec.Rules[i].Host, "*", "star"),
		}
	}
	// create a new status
	ingress.Status = v1.IngressStatus{
		LoadBalancer: corev1.LoadBalancerStatus{
			Ingress: hosts,
		},
	}
	// send the update to k8s
	_, err := l.client.NetworkingV1().Ingresses(ingress.GetNamespace()).UpdateStatus(ctx, ingress, metav1.UpdateOptions{
		FieldManager: DefaultControllerName,
	})
	if err != nil {
		span.RecordError(err)
		log.WithError(err).WithContext(ctx).WithFields(fields).Error("failed to update Ingress status")
		return
	}
	log.WithContext(ctx).WithFields(fields).Info("successfully updated Ingress status")
}

// Delete is a no-op
func (l *StatusListener) Delete(ctx context.Context, ingress *v1.Ingress) {
	_, span := otel.Tracer(tracing.DefaultServiceName).Start(ctx, "listener_status_delete", trace.WithAttributes(
		attribute.String("name", ingress.GetName()),
		attribute.String("namespace", ingress.GetNamespace()),
	))
	span.End()
}
