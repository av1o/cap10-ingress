package ingress

import (
	"context"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/av1o/cap10-ingress/pkg/tracing"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
	v1 "k8s.io/api/networking/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/wait"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/util/retry"
	"time"
)

const (
	DefaultControllerName     = "dcas.dev/ingress-controller"
	DeprecatedClassAnnotation = "kubernetes.io/ingress.class"
	DefaultClassAnnotation    = "ingressclass.kubernetes.io/is-default-class"
)

type Reader struct {
	client kubernetes.Interface
}

func NewReader(client kubernetes.Interface) *Reader {
	return &Reader{
		client: client,
	}
}

func (r *Reader) GetClass(ctx context.Context, name string) (*v1.IngressClass, error) {
	ctx, span := otel.Tracer(tracing.DefaultServiceName).Start(ctx, "reader_class_getClass", trace.WithAttributes(
		attribute.String("name", name),
	))
	defer span.End()
	fields := log.Fields{"name": name}
	log.WithContext(ctx).WithFields(fields).Debug("attempting to fetch IngressClass")
	class, err := r.client.NetworkingV1().IngressClasses().Get(ctx, name, metav1.GetOptions{})
	if err != nil {
		span.RecordError(err)
		log.WithError(err).WithContext(ctx).WithFields(fields).Error("failed to retrieve IngressClass")
		return nil, err
	}
	return class, nil
}

// RetryGetClass calls GetClass with a RetryBackoff.
//
// It will attempt 4 times with an increasing delay
// unless the error is a permission problem that needs
// to be fixed by the operator.
func (r *Reader) RetryGetClass(ctx context.Context, name string) (*v1.IngressClass, error) {
	ctx, span := otel.Tracer(tracing.DefaultServiceName).Start(ctx, "reader_class_getClass", trace.WithAttributes(
		attribute.String("name", name),
	))
	defer span.End()
	log.WithContext(ctx).Infof("attempting to fetch IngressClass: '%s'", name)
	var class *v1.IngressClass
	steps := 4
	// retry a couple of times
	if err := retry.OnError(wait.Backoff{
		Steps:    steps,
		Duration: time.Second,
		Factor:   3.0,
		Jitter:   0.1,
	}, func(err error) bool {
		// if we're forbidden there's not much we can do
		return !errors.IsForbidden(err)
	}, func() error {
		// try fetching the class
		c, err := r.GetClass(ctx, name)
		if err != nil {
			return err
		}
		class = c
		return nil
	}); err != nil {
		span.RecordError(fmt.Errorf("reached max attempts after %d attempts", steps))
		log.WithError(err).Error("failed max allowed attempts to fetch the IngressClass")
		return nil, err
	}
	return class, nil
}
