package ingress

import (
	"context"
	v1 "k8s.io/api/networking/v1"
)

type ChangeListener interface {
	Add(ctx context.Context, ingress *v1.Ingress)
	Delete(ctx context.Context, ingress *v1.Ingress)
}
