package ingress

import (
	"context"
	"github.com/stretchr/testify/assert"
	v1 "k8s.io/api/networking/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/utils/pointer"
	"testing"
)

func TestController_shouldIgnore(t *testing.T) {
	ctx := context.TODO()
	var cases = []struct {
		name string
		c    *Controller
		in   *v1.Ingress
		out  bool
	}{
		{
			"default class allows nil",
			&Controller{ingressDefault: true, ingressClassName: "foobar"},
			&v1.Ingress{
				Spec: v1.IngressSpec{IngressClassName: nil},
			},
			false,
		},
		{
			"default class allows match",
			&Controller{ingressDefault: true, ingressClassName: "foobar"},
			&v1.Ingress{
				Spec: v1.IngressSpec{IngressClassName: pointer.StringPtr("foobar")},
			},
			false,
		},
		{
			"class allows legacy match",
			&Controller{ingressDefault: false, ingressClassName: "foobar"},
			&v1.Ingress{
				ObjectMeta: metav1.ObjectMeta{
					Annotations: map[string]string{
						DeprecatedClassAnnotation: "foobar",
					},
				},
				Spec: v1.IngressSpec{IngressClassName: nil},
			},
			false,
		},
		{
			"class allows match",
			&Controller{ingressDefault: false, ingressClassName: "foobar"},
			&v1.Ingress{
				Spec: v1.IngressSpec{IngressClassName: pointer.StringPtr("foobar")},
			},
			false,
		},
		{
			"class ignores others",
			&Controller{ingressDefault: false, ingressClassName: "foobar"},
			&v1.Ingress{
				Spec: v1.IngressSpec{IngressClassName: pointer.StringPtr("barfoo")},
			},
			true,
		},
		{
			"class ignores legacy others",
			&Controller{ingressDefault: false, ingressClassName: "foobar"},
			&v1.Ingress{
				ObjectMeta: metav1.ObjectMeta{
					Annotations: map[string]string{
						DeprecatedClassAnnotation: "barfoo",
					},
				},
				Spec: v1.IngressSpec{IngressClassName: nil},
			},
			true,
		},
		{
			"class ignores nil",
			&Controller{ingressDefault: false, ingressClassName: "foobar"},
			&v1.Ingress{
				Spec: v1.IngressSpec{IngressClassName: nil},
			},
			true,
		},
	}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			assert.EqualValues(t, tt.out, tt.c.shouldIgnore(ctx, tt.in))
		})
	}
}
