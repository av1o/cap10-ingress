package ingress

import (
	log "github.com/sirupsen/logrus"
	"k8s.io/client-go/informers"
	"k8s.io/client-go/kubernetes"
	"time"
)

func Init(scope string, clientset kubernetes.Interface, listeners []ChangeListener, ingressClassName string, ingressDefault bool) chan struct{} {
	// start the watcher
	var factory informers.SharedInformerFactory
	if scope != "" {
		log.WithField("scope", scope).Info("creating scoped informer")
		factory = informers.NewSharedInformerFactoryWithOptions(clientset, time.Minute, informers.WithNamespace(scope))
	} else {
		log.Info("creating global informer")
		factory = informers.NewSharedInformerFactory(clientset, time.Minute)
	}
	ctrl := NewController(factory, listeners, ingressClassName, ingressDefault)
	stopper := make(chan struct{})
	err := ctrl.Start(stopper)
	if err != nil {
		log.WithError(err).Fatal("failed to start controller")
		close(stopper)
		return nil
	}
	return stopper
}
