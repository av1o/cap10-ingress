package ingress_test

import (
	"context"
	"github.com/stretchr/testify/assert"
	"gitlab.com/av1o/cap10-ingress/internal/k8s/ingress"
	v1 "k8s.io/api/networking/v1"
	corev1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes/fake"
	"testing"
)

func TestNewStatusListener(t *testing.T) {
	assert.NotNil(t, ingress.NewStatusListener(nil))
}

func TestStatusListener_Add(t *testing.T) {
	ctx := context.TODO()
	ing := &v1.Ingress{
		ObjectMeta: corev1.ObjectMeta{
			Name:      "foo",
			Namespace: "default",
		},
		Spec: v1.IngressSpec{
			Rules: []v1.IngressRule{
				{
					Host: "foo.bar",
				},
				{
					Host: "example.org",
				},
			},
		},
	}
	client := fake.NewSimpleClientset(ing)
	l := ingress.NewStatusListener(client)
	l.Add(ctx, ing)

	resp, err := client.NetworkingV1().Ingresses("default").Get(context.TODO(), "foo", corev1.GetOptions{})
	assert.NoError(t, err)
	assert.Len(t, resp.Status.LoadBalancer.Ingress, 2)
}

func TestStatusListener_Delete(t *testing.T) {
	ing := &v1.Ingress{
		ObjectMeta: corev1.ObjectMeta{
			Name:      "foo",
			Namespace: "default",
		},
		Spec: v1.IngressSpec{
			Rules: []v1.IngressRule{
				{
					Host: "foo.bar",
				},
				{
					Host: "example.org",
				},
			},
		},
	}
	client := fake.NewSimpleClientset(ing)
	l := ingress.NewStatusListener(client)
	assert.NotPanics(t, func() {
		l.Delete(context.TODO(), ing)
	})
}
