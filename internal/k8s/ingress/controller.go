package ingress

import (
	"context"
	"errors"
	log "github.com/sirupsen/logrus"
	"gitlab.com/av1o/cap10-ingress/pkg/tracing"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
	netv1 "k8s.io/api/networking/v1"
	"k8s.io/client-go/informers"
	v1 "k8s.io/client-go/informers/networking/v1"
	"k8s.io/client-go/tools/cache"
	"time"
)

type Controller struct {
	informerFactory  informers.SharedInformerFactory
	netInformer      v1.IngressInformer
	listeners        []ChangeListener
	ingressClassName string
	ingressDefault   bool
}

// NewController creates a new Controller.
func NewController(informerFactory informers.SharedInformerFactory, listeners []ChangeListener, ingressClassName string, ingressDefault bool) *Controller {
	log.Infof("creating new controller (%s) with %d listeners (default: %+v)", ingressClassName, len(listeners), ingressDefault)
	ingInformer := informerFactory.Networking().V1().Ingresses()
	c := &Controller{
		informerFactory:  informerFactory,
		netInformer:      ingInformer,
		listeners:        listeners,
		ingressClassName: ingressClassName,
		ingressDefault:   ingressDefault,
	}
	c.netInformer.Informer().AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc:    c.add,
		UpdateFunc: c.update,
		DeleteFunc: c.delete,
	})
	return c
}

func (c *Controller) Start(stopper chan struct{}) error {
	log.Info("starting watcher")
	// start all the informers created by this factory
	c.informerFactory.Start(stopper)
	// wait for the local cache to sync
	start := time.Now()
	log.Info("waiting for cache to sync...")
	if !cache.WaitForCacheSync(stopper, c.netInformer.Informer().HasSynced) {
		return errors.New("failed to sync")
	}
	log.Infof("cache synced successfully in %s", time.Since(start))
	return nil
}

func (c *Controller) shouldIgnore(ctx context.Context, ing *netv1.Ingress) bool {
	ctx, span := otel.Tracer(tracing.DefaultServiceName).Start(ctx, "controller_shouldIgnore", trace.WithAttributes(
		attribute.String("name", ing.GetName()),
		attribute.String("namespace", ing.GetNamespace()),
	))
	defer span.End()
	// if the deprecated annotation is set to a different name
	// we ignore it
	if val, ok := ing.ObjectMeta.Annotations[DeprecatedClassAnnotation]; ok && val != c.ingressClassName {
		return true
	} else if val == c.ingressClassName {
		log.WithContext(ctx).Warningf("detected deprecated annotation - please set the .spec.ingressClassName field on your Ingress")
		return false
	}
	// if the ingress has no class name, and
	// we're not the default - ignore it
	if ing.Spec.IngressClassName == nil && !c.ingressDefault {
		return true
	} else if c.ingressDefault {
		return false
	}
	// if the classname is set, and it
	// doesn't match ours - ignore it
	return *ing.Spec.IngressClassName != c.ingressClassName
}

func (c *Controller) add(obj interface{}) {
	ing := obj.(*netv1.Ingress)
	ctx, span := otel.Tracer(tracing.DefaultServiceName).Start(context.TODO(), "controller_add", trace.WithAttributes(
		attribute.String("name", ing.GetName()),
		attribute.String("namespace", ing.GetNamespace()),
	))
	defer span.End()
	ignored := c.shouldIgnore(ctx, ing)
	log.Infof("create: %s/%s (ignored: %+v)", ing.GetNamespace(), ing.GetName(), ignored)
	if ignored {
		return
	}
	for _, l := range c.listeners {
		l.Add(ctx, ing)
	}
}

func (c *Controller) update(_, new interface{}) {
	ing := new.(*netv1.Ingress)
	ctx, span := otel.Tracer(tracing.DefaultServiceName).Start(context.TODO(), "controller_update", trace.WithAttributes(
		attribute.String("name", ing.GetName()),
		attribute.String("namespace", ing.GetNamespace()),
	))
	defer span.End()
	ignored := c.shouldIgnore(ctx, ing)
	log.Infof("update: %s/%s (ignored: %+v)", ing.GetNamespace(), ing.GetName(), ignored)
	if ignored {
		return
	}
	for _, l := range c.listeners {
		l.Add(ctx, ing)
	}
}

func (c *Controller) delete(obj interface{}) {
	ing := obj.(*netv1.Ingress)
	ctx, span := otel.Tracer(tracing.DefaultServiceName).Start(context.TODO(), "controller_delete", trace.WithAttributes(
		attribute.String("name", ing.GetName()),
		attribute.String("namespace", ing.GetNamespace()),
	))
	defer span.End()
	ignored := c.shouldIgnore(ctx, ing)
	log.Infof("delete: %s/%s (ignored: %+v)", ing.GetNamespace(), ing.GetName(), ignored)
	if ignored {
		return
	}
	for _, l := range c.listeners {
		l.Delete(ctx, ing)
	}
}
