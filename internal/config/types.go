package config

import "fmt"

const (
	DefaultIngressClassName = "cap10"
	DefaultClusterDomain    = "cluster.local"
)

type HSTS struct {
	Enabled           bool  `yaml:"enabled"`
	IncludeSubdomains bool  `yaml:"include_subdomains"`
	Preload           bool  `yaml:"preload"`
	MaxAge            int64 `yaml:"max_age"`
}

func (h *HSTS) String() string {
	val := fmt.Sprintf("max-age=%d", h.MaxAge)
	if h.IncludeSubdomains {
		val += "; includeSubDomains"
	}
	if h.Preload {
		val += "; preload"
	}
	return val
}

type Transport struct {
	MaxConnsPerHost     int `yaml:"max_conns_per_host"`
	MaxIdleConns        int `yaml:"max_idle_conns"`
	MaxIdleConnsPerHost int `yaml:"max_idle_conns_per_host"`
}

type tls struct {
	DefaultCertificate string `yaml:"default_certificate"`
}

type kube struct {
	ClusterDomain string `yaml:"cluster_domain"`
}

type otel struct {
	Enabled    bool    `yaml:"enabled"`
	SampleRate float64 `yaml:"sample_rate"`
}

type Root struct {
	HSTS              HSTS      `yaml:"hsts"`
	TLS               tls       `yaml:"tls"`
	Transport         Transport `yaml:"transport"`
	Kube              kube      `yaml:"kube"`
	IngressClassName  string    `yaml:"ingress_class_name"`
	TCP               TCPConfig `yaml:"tcp"`
	OpenTelemetry     otel      `yaml:"open_telemetry"`
	AllowSNISmuggling bool      `yaml:"allow_sni_smuggling"`
}

type TCPConfig map[uint16]string
