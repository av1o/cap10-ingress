package config

import (
	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v3"
	"io/ioutil"
	"net/http"
)

func defaultConfig() *Root {
	return &Root{
		HSTS: HSTS{
			Enabled:           true,
			IncludeSubdomains: true,
			MaxAge:            63072000,
		},
		TLS: tls{},
		Kube: kube{
			ClusterDomain: DefaultClusterDomain,
		},
		IngressClassName: DefaultIngressClassName,
		TCP:              TCPConfig{},
		OpenTelemetry:    otel{},
		Transport: Transport{
			MaxConnsPerHost:     0,
			MaxIdleConns:        0,
			MaxIdleConnsPerHost: http.DefaultMaxIdleConnsPerHost,
		},
		AllowSNISmuggling: false,
	}
}

func Read(path string) (*Root, error) {
	log.Debugf("reading config from path: '%s'", path)
	// read the file
	data, err := ioutil.ReadFile(path)
	if err != nil {
		log.WithError(err).Errorf("failed to read file: '%s'", path)
		return nil, err
	}
	conf := defaultConfig()
	err = yaml.Unmarshal(data, conf)
	if err != nil {
		log.WithError(err).Error("failed to unmarshal yaml")
		return nil, err
	}
	log.Debugf("successfully read config from file: '%s'", path)
	return conf, nil
}
