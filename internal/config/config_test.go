package config

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestRead(t *testing.T) {
	var cases = []struct {
		in  string
		err bool
	}{
		{
			"./testdata/valid.yaml",
			false,
		},
		{
			"./testdata/invalid.yaml",
			true,
		},
		{
			"./testdata/invalid-port.yaml",
			true,
		},
		{
			t.TempDir(),
			true,
		},
	}
	for _, tt := range cases {
		t.Run(tt.in, func(t *testing.T) {
			conf, err := Read(tt.in)
			if tt.err {
				assert.Error(t, err)
				assert.Nil(t, conf)
			} else {
				assert.NoError(t, err)
				assert.NotNil(t, conf)
			}
		})
	}
}

func TestDefaults(t *testing.T) {
	conf, err := Read("./testdata/valid.yaml")
	assert.NoError(t, err)
	assert.EqualValues(t, "cert-manager/foobar", conf.TLS.DefaultCertificate)
}
