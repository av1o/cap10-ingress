package config

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestHSTS_String(t *testing.T) {
	var cases = []struct {
		conf *HSTS
		out  string
	}{
		{
			&HSTS{
				IncludeSubdomains: false,
				Preload:           false,
				MaxAge:            31536000,
			},
			"max-age=31536000",
		},
		{
			&HSTS{
				IncludeSubdomains: false,
				Preload:           true,
				MaxAge:            31536000,
			},
			"max-age=31536000; preload",
		},
		{
			&HSTS{
				IncludeSubdomains: true,
				Preload:           true,
				MaxAge:            31536000,
			},
			"max-age=31536000; includeSubDomains; preload",
		},
	}
	for _, tt := range cases {
		t.Run(tt.out, func(t *testing.T) {
			assert.EqualValues(t, tt.out, tt.conf.String())
		})
	}
}
