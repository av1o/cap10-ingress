package sni

import (
	"crypto/tls"
	"fmt"
	"github.com/stretchr/testify/assert"
	"k8s.io/apimachinery/pkg/util/rand"
	"net"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
)

func TestMuxListener(t *testing.T) {
	// start the fake TLS backend
	ts := httptest.NewTLSServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, _ = w.Write([]byte("OK"))
	}))
	defer ts.Close()

	port := rand.IntnRange(10000, 50000)
	l, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	assert.NoError(t, err)

	uri, _ := url.Parse(ts.URL)
	httpListener := MuxListener(l, func(host string) (*ProxyConfig, bool) {
		if host == "localhost" {
			return &ProxyConfig{
				Addr: uri.Host,
			}, true
		}
		return nil, false
	})

	// start the http server
	go func() {
		srv := httptest.NewUnstartedServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(http.StatusForbidden)
			_, _ = w.Write([]byte("Forbidden."))
		}))
		srv.Listener = httpListener
		srv.StartTLS()
	}()

	client := &http.Client{
		Transport: &http.Transport{
			ForceAttemptHTTP2: true,
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	}

	t.Run("correct SNI reaches backend", func(t *testing.T) {
		resp, err := client.Get(fmt.Sprintf("https://localhost:%d", port))
		assert.NoError(t, err)
		t.Log(resp.Status)
		assert.EqualValues(t, http.StatusOK, resp.StatusCode)
	})
	t.Run("invalid SNI continues to HTTP server", func(t *testing.T) {
		resp, err := client.Get(fmt.Sprintf("https://127.0.0.1:%d", port))
		assert.NoError(t, err)
		t.Log(resp.Status)
		assert.EqualValues(t, http.StatusForbidden, resp.StatusCode)
	})
}
