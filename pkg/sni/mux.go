package sni

import (
	"bufio"
	"bytes"
	"crypto/tls"
	"errors"
	log "github.com/sirupsen/logrus"
	"io"
	"net"
	"sync"
	"time"
)

const recordHeaderLen = 5

type ProxyConfig struct {
	Addr string
}

type GetConfigOptions = func(host string) (*ProxyConfig, bool)

func MuxListener(l net.Listener, opt GetConfigOptions) net.Listener {
	other := newListener(l)
	go func() {
		for {
			sconn, err := l.Accept()
			if err != nil {
				log.WithError(err).Error("failed to accept connection")
				continue
			}
			if err := sconn.SetReadDeadline(time.Now().Add(time.Second * 10)); err != nil {
				log.WithError(err).Error("failed to set read deadline")
				continue
			}
			header := bufio.NewReader(sconn)
			sni, hdr := clientHelloServerName(header)
			log.Infof("intercepted SNI: '%s'", sni)
			if sni == "" {
				log.Warningf("received request without Service Name Indication")
			}
			nconn := conn{
				Name:   sni,
				Peeked: hdr,
				Conn:   sconn,
			}
			// check if we should
			// be intercepting the connection
			cfg, ok := opt(sni)
			if ok && cfg != nil {
				_ = proxyConn(sconn, &nconn, cfg.Addr)
				continue
			}
			// assume the request is HTTP and
			// pass it off to that server
			log.Infof("returning HTTP connection for '%s'", sni)
			if other.accept != nil {
				other.accept <- &nconn
			}
		}
	}()
	return other
}

func proxyConn(src net.Conn, dst *conn, addr string) error {
	log.Infof("proxying TCP connection to %s", addr)
	c, err := net.DialTimeout("tcp", addr, time.Second*5)
	if err != nil {
		log.WithError(err).Error("failed to open backend connection")
		return err
	}
	defer c.Close()
	var wg sync.WaitGroup
	wg.Add(2)

	go func() {
		_, _ = io.Copy(src, c)
		_ = src.(*net.TCPConn).CloseWrite()
		wg.Done()
	}()
	go func() {
		_, _ = io.Copy(c, dst)
		_ = c.(*net.TCPConn).CloseWrite()
		wg.Done()
	}()
	wg.Wait()
	return nil
}

// clientHelloServerName returns the SNI server name inside the TLS ClientHello,
// without consuming any bytes from br.
// On any error, the empty string is returned.
func clientHelloServerName(br *bufio.Reader) (sni string, hdr []byte) {
	hdr, err := br.Peek(recordHeaderLen)
	if err != nil {
		log.WithError(err).Error("failed to peek tls header")
		return "", nil
	}
	const recordTypeHandshake = 0x16
	if hdr[0] != recordTypeHandshake {
		log.Warningf("skipping non-TLS request: %s", string(hdr))
		return "", nil // Not TLS.
	}
	recLen := int(hdr[3])<<8 | int(hdr[4]) // ignoring version in hdr[1:3]
	helloBytes, err := br.Peek(recordHeaderLen + recLen)
	if err != nil {
		log.WithError(err).Error("failed to peek extended tls header")
		return "", hdr
	}
	hdr = helloBytes
	// ignore the error here because
	// it will always complain about certificates
	_ = tls.Server(sniSniffConn{r: bytes.NewReader(helloBytes)}, &tls.Config{
		GetConfigForClient: func(hello *tls.ClientHelloInfo) (*tls.Config, error) {
			sni = hello.ServerName
			log.Infof("fetched SNI: %s", sni)
			return nil, nil
		},
	}).Handshake()
	return
}

type listener struct {
	accept chan net.Conn
	net.Listener
}

func newListener(l net.Listener) *listener {
	return &listener{
		make(chan net.Conn),
		l,
	}
}

func (l *listener) Accept() (net.Conn, error) {
	if l.accept == nil {
		return nil, errors.New("listener closed")
	}
	return <-l.accept, nil
}

func (l *listener) Close() error {
	close(l.accept)
	l.accept = nil
	return nil
}
