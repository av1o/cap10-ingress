package tracing

import (
	"context"
	log "github.com/sirupsen/logrus"
	"go.opentelemetry.io/otel/attribute"
	"os"
	"os/signal"
	"runtime"
	"syscall"

	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/exporters/jaeger"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/sdk/resource"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.4.0"
)

// Init configures OpenTelemetry so that it will
// send traces to a Jaeger instance.
//
// It sets the global providers so that other libraries
// will send their traces to the correct location.
func Init(opts OtelOptions) error {
	log.WithFields(log.Fields{
		"service":     opts.ServiceName,
		"environment": opts.Environment,
		"sampleRate":  opts.SampleRate,
	}).Info("enabling OpenTelemetry trace collection")
	if url := os.Getenv("OTEL_EXPORTER_JAEGER_ENDPOINT"); url == "" {
		log.Info("skipping OpenTelemetry integration as no Jaeger endpoint has been configured")
		return nil
	}
	exporter, err := jaeger.New(jaeger.WithCollectorEndpoint())
	if err != nil {
		log.WithError(err).Error("failed to setup otel exporter")
		return err
	}
	host, err := os.Hostname()
	if err != nil {
		log.WithError(err).Error("failed to retrieve system hostname - this may cause strange tracing output")
		host = "unknown"
	}
	tp := sdktrace.NewTracerProvider(
		sdktrace.WithBatcher(exporter),
		sdktrace.WithSampler(sdktrace.TraceIDRatioBased(opts.SampleRate)),
		sdktrace.WithResource(resource.NewWithAttributes(
			semconv.SchemaURL,
			semconv.ServiceNameKey.String(opts.ServiceName),
			attribute.String(TagEnv, opts.Environment),
			attribute.String(TagOS, runtime.GOOS),
			attribute.String(TagArch, runtime.GOARCH),
			attribute.String(TagHost, host),
		)),
	)
	otel.SetTracerProvider(tp)
	otel.SetTextMapPropagator(propagation.NewCompositeTextMapPropagator(propagation.TraceContext{}, propagation.Baggage{}))

	// shutdown when the app does
	go waitForShutdown(tp)

	return nil
}

// waitForShutdown waits until the app is being shutdown
// before closing the sdktrace.TracerProvider
func waitForShutdown(tp *sdktrace.TracerProvider) {
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGTERM, syscall.SIGINT)
	<-sig
	if err := tp.Shutdown(context.Background()); err != nil {
		log.WithError(err).Error("error stopping otel tracer provider")
	}
}
