package tracing

const (
	TagEnv  = "env"
	TagOS   = "os"
	TagArch = "arch"
	TagHost = "host"
)

const DefaultServiceName = "cap10-ingress"

type OtelOptions struct {
	ServiceName string
	Environment string
	SampleRate  float64
}
