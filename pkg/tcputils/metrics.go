package tcputils

import (
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/av1o/cap10-ingress/internal/metrics"
)

var (
	requestTags = []string{
		"port",
		"destination",
	}
)

type TCPProxyCollector struct {
	prometheus.Collector

	responseTime   *prometheus.HistogramVec
	responseLength *prometheus.HistogramVec

	requestTime   *prometheus.HistogramVec
	requestLength *prometheus.HistogramVec

	upstreamLatency *prometheus.SummaryVec

	requests *prometheus.CounterVec
}

func NewTCPProxyCollector(pod, namespace string) *TCPProxyCollector {
	defaultLabels := prometheus.Labels{
		"controller_pod":       pod,
		"controller_namespace": namespace,
	}
	return &TCPProxyCollector{
		requestTime: prometheus.NewHistogramVec(prometheus.HistogramOpts{
			Name:        "tcp_request_duration_seconds",
			Help:        "TCP request time",
			Namespace:   metrics.PrometheusNamespace,
			ConstLabels: defaultLabels,
			Buckets:     metrics.TimeBuckets,
		}, requestTags),
		requestLength: prometheus.NewHistogramVec(prometheus.HistogramOpts{
			Name:        "tcp_request_size",
			Help:        "TCP request size",
			Namespace:   metrics.PrometheusNamespace,
			ConstLabels: defaultLabels,
			Buckets:     metrics.LengthBuckets,
		}, requestTags),
		responseTime: prometheus.NewHistogramVec(prometheus.HistogramOpts{
			Name:        "tcp_response_duration_seconds",
			Help:        "TCP response time",
			Namespace:   metrics.PrometheusNamespace,
			ConstLabels: defaultLabels,
			Buckets:     metrics.TimeBuckets,
		}, requestTags),
		responseLength: prometheus.NewHistogramVec(prometheus.HistogramOpts{
			Name:        "tcp_response_size",
			Help:        "TCP response size",
			Namespace:   metrics.PrometheusNamespace,
			ConstLabels: defaultLabels,
			Buckets:     metrics.LengthBuckets,
		}, requestTags),
		upstreamLatency: prometheus.NewSummaryVec(prometheus.SummaryOpts{
			Name:        "tcp_upstream_latency_seconds",
			Help:        "Upstream service latency per TCP proxy",
			Namespace:   metrics.PrometheusNamespace,
			Objectives:  metrics.DefaultObjectives,
			ConstLabels: defaultLabels,
		}, requestTags),
		requests: prometheus.NewCounterVec(prometheus.CounterOpts{
			Name:        "tcp_requests",
			Help:        "Total number of client requests",
			Namespace:   metrics.PrometheusNamespace,
			ConstLabels: defaultLabels,
		}, []string{"src", "dst", "addr"}),
	}
}

func (c TCPProxyCollector) Describe(ch chan<- *prometheus.Desc) {
	c.requestTime.Describe(ch)
	c.requestLength.Describe(ch)

	c.responseTime.Describe(ch)
	c.responseLength.Describe(ch)

	c.upstreamLatency.Describe(ch)

	c.requests.Describe(ch)
}

func (c TCPProxyCollector) Collect(ch chan<- prometheus.Metric) {
	c.requestTime.Collect(ch)
	c.requestLength.Collect(ch)

	c.responseTime.Collect(ch)
	c.responseLength.Collect(ch)

	c.upstreamLatency.Collect(ch)

	c.requests.Collect(ch)
}
