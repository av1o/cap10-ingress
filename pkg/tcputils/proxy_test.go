package tcputils

import (
	"bufio"
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/av1o/cap10-ingress/internal/config"
	"k8s.io/apimachinery/pkg/util/rand"
	"net"
	"strings"
	"testing"
	"time"
)

func TestNewDest(t *testing.T) {
	var cases = []struct {
		in  string
		out string
		err bool
	}{
		{
			"default/go-example:8080",
			"go-example.default.svc.cluster.local:8080",
			false,
		},
		{
			"go-example:8080",
			"",
			true,
		},
		{
			"default/go-example",
			"",
			true,
		},
		{
			"default/go-example:http",
			"",
			true,
		},
	}

	for _, tt := range cases {
		t.Run(tt.in, func(t *testing.T) {
			out, err := NewDest(tt.in, config.DefaultClusterDomain)
			assert.EqualValues(t, tt.out, out)
			if tt.err {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}

func newListener(t *testing.T) net.Addr {
	ln, err := net.Listen("tcp", "127.0.0.1:0")
	assert.NoError(t, err)
	go func() {
		for {
			conn, err := ln.Accept()
			if err != nil {
				log.WithError(err).Error("error accepting connection")
				continue
			}
			scanner := bufio.NewScanner(conn)
			for scanner.Scan() {
				message := scanner.Text()
				fmt.Println("Message Received:", message)
				newMessage := strings.ToUpper(message)
				_, _ = conn.Write([]byte(newMessage + "\n"))
			}
		}
	}()
	t.Logf("starting fake server on addr: %s", ln.Addr())
	return ln.Addr()
}

func TestTCPProxy_Serve(t *testing.T) {
	log.SetLevel(log.DebugLevel)

	dst := newListener(t)
	port := rand.IntnRange(1025, 65534)
	proxy := NewTCPProxy(uint16(port), dst.String(), NewTCPProxyCollector("some-pod-arst", "default"))
	go func() {
		_ = proxy.Serve()
	}()

	time.Sleep(time.Second)

	conn, err := net.Dial("tcp", fmt.Sprintf(":%d", port))
	assert.NoError(t, err)
	defer conn.Close()
	bw, err := conn.Write([]byte("Hello, World!"))
	assert.NoError(t, err)
	t.Log(bw)
	assert.NotZero(t, bw)
}
