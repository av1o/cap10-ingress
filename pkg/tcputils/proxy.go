package tcputils

import (
	"errors"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/av1o/cap10-ingress/internal/config"
	"io"
	"k8s.io/apimachinery/pkg/util/wait"
	"k8s.io/client-go/util/retry"
	"net"
	"strconv"
	"strings"
	"sync"
	"time"
)

func NewDest(val, domain string) (string, error) {
	bits := strings.SplitN(val, "/", 2)
	if len(bits) != 2 {
		log.Errorf("invalid destination (expecting namespace/name:port, got: '%s'", val)
		return "", errors.New("invalid number of segments")
	}
	namespace := bits[0]
	bits = strings.SplitN(bits[1], ":", 2)
	if len(bits) != 2 {
		log.Errorf("invalid service description (expecting [name, port], got '%+v')", bits)
		return "", errors.New("invalid service name")
	}
	port, err := strconv.Atoi(bits[1])
	if err != nil {
		log.WithError(err).Errorf("failed to parse integer from port: '%s'", bits[1])
		return "", err
	}
	return fmt.Sprintf("%s.%s.svc.%s:%d", bits[0], namespace, domain, port), nil
}

type TCPProxy struct {
	srcPort      uint16
	dst          string
	proxyMetrics *TCPProxyCollector
}

func NewProxiesFromConfig(clusterDomain string, conf config.TCPConfig, proxyMetrics *TCPProxyCollector) error {
	var dst string
	var err error
	log.Infof("starting %d tcp proxies", len(conf))
	for k, v := range conf {
		dst, err = NewDest(v, clusterDomain)
		if err != nil {
			return err
		}
		go NewTCPProxy(k, dst, proxyMetrics).Serve()
	}
	return nil
}

func NewTCPProxy(srcPort uint16, dst string, proxyMetrics *TCPProxyCollector) *TCPProxy {
	return &TCPProxy{
		srcPort:      srcPort,
		dst:          dst,
		proxyMetrics: proxyMetrics,
	}
}

// dial attempts to establish a tcp connection to the
// upstream.
//
// It will retry on error
func (p *TCPProxy) dial() (net.Conn, error) {
	var conn net.Conn
	if err := retry.OnError(wait.Backoff{
		Steps:    4,
		Duration: time.Second,
		Factor:   3.0,
		Jitter:   0.5,
	}, func(err error) bool {
		return err != nil
	}, func() error {
		log.Debugf("dialing remote: %s", p.dst)
		start := time.Now()
		dst, err := net.Dial("tcp", p.dst)
		// collect metrics even if it failed
		dur := time.Since(start)
		p.proxyMetrics.upstreamLatency.WithLabelValues(strconv.Itoa(int(p.srcPort)), p.dst).Observe(float64(dur.Milliseconds()))
		if err != nil {
			log.WithError(err).Error("failed to dial remote")
			return err
		}
		conn = dst
		return nil
	}); err != nil {
		log.WithError(err).Error("failed max attempts to dial the upstream")
		return nil, err
	}
	return conn, nil
}

func (p *TCPProxy) Serve() error {
	// open connection
	log.Infof("establishing tcp listener on port %d", p.srcPort)
	listener, err := net.Listen("tcp", fmt.Sprintf(":%d", p.srcPort))
	if err != nil {
		log.WithError(err).Error("failed to open listener")
		return err
	}
	for {
		// wait for something to come in
		conn, err := listener.Accept()
		if err != nil {
			log.WithError(err).Error("failed to accept connection")
			continue
		}
		log.Infof("TCP ? %s [%s]", conn.RemoteAddr(), p.dst)
		log.Debugf("accepted connection from %s", conn.RemoteAddr())
		go func() {
			// dial the destination
			dst, err := p.dial()
			if err != nil {
				log.WithError(conn.Close()).Debug("closing source connection")
				return
			}
			p.proxyMetrics.requests.WithLabelValues(strconv.Itoa(int(p.srcPort)), p.dst, conn.RemoteAddr().String()).Inc()
			var wg sync.WaitGroup
			wg.Add(2)

			go func() {
				start := time.Now()
				// copy data there
				bw, err := io.Copy(dst, conn)
				dur := time.Since(start)
				p.proxyMetrics.requestLength.WithLabelValues(strconv.Itoa(int(p.srcPort)), p.dst).Observe(float64(bw))
				p.proxyMetrics.requestTime.WithLabelValues(strconv.Itoa(int(p.srcPort)), p.dst).Observe(float64(dur.Milliseconds()))
				log.WithError(err).Debugf("copied %d bytes from src to dst in %s", bw, dur)
				wg.Done()
			}()
			go func() {
				start := time.Now()
				// copy data back
				bw, err := io.Copy(conn, dst)
				dur := time.Since(start)
				p.proxyMetrics.responseLength.WithLabelValues(strconv.Itoa(int(p.srcPort)), p.dst).Observe(float64(bw))
				p.proxyMetrics.responseTime.WithLabelValues(strconv.Itoa(int(p.srcPort)), p.dst).Observe(float64(dur.Milliseconds()))
				log.WithError(err).Debugf("copied %d bytes from dst to src in %s", bw, dur)
				wg.Done()
			}()
			wg.Wait()
			log.WithError(dst.Close()).Debug("closing destination connection")
			log.WithError(conn.Close()).Debug("closing source connection")
		}()
	}
}
