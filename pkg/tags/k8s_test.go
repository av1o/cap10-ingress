package tags

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestMarshal(t *testing.T) {
	var item = struct {
		String    string `k8s:"foo.bar.io"`
		Number    int    `k8s:"foo.bar/zoo"`
		Number2   int    `k8s:"foo.bar/foo"`
		Something string
		IsFoo     bool `k8s:"is-foo"`
	}{}

	err := Marshal(&item, map[string]string{
		"foo.bar.io":  "foo",
		"foo.bar/zoo": "23",
		"is-foo":      "true",
	})
	assert.NoError(t, err)
	assert.EqualValues(t, item.String, "foo")
	assert.EqualValues(t, item.Number, 23)
	assert.EqualValues(t, item.Something, "")
	assert.EqualValues(t, item.Number2, 0)
	assert.True(t, item.IsFoo)
}

func TestMarshal_default(t *testing.T) {
	var item = struct {
		Foo string `k8s:"foo" default:"A"`
		Bar string `k8s:"foo"`
	}{}

	err := Marshal(&item, map[string]string{})
	assert.NoError(t, err)
	assert.EqualValues(t, "A", item.Foo)
	assert.Empty(t, item.Bar)
}

func TestMarshal_multi(t *testing.T) {
	var item = struct {
		Foo string `k8s:"foo,bar"`
	}{}

	err := Marshal(&item, map[string]string{
		"bar": "arst",
	})
	assert.NoError(t, err)
	assert.EqualValues(t, "arst", item.Foo)
}
