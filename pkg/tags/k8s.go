package tags

import (
	"reflect"
	"strconv"
	"strings"
)

const (
	TagName    = "k8s"
	TagDefault = "default"
)

// Marshal extracts values from Kubernetes annotations
// into a given struct pointer.
//
// Currently, supports string, int and bool
func Marshal(v interface{}, data map[string]string) error {
	e := reflect.ValueOf(v).Elem()
	typ := e.Type()
	for i := 0; i < e.NumField(); i++ {
		field := e.Field(i)
		// check that we're allowed to set this field
		if !field.CanSet() {
			continue
		}
		fieldType := typ.Field(i)
		// lookup the tag
		tag, ok := fieldType.Tag.Lookup(TagName)
		if !ok {
			continue
		}
		defaultValue := fieldType.Tag.Get(TagDefault)
		val := getValue(tag, data)
		// use the default value if we got nothing
		if val == "" {
			val = defaultValue
		}
		// figure out what conversions we need to do
		switch field.Kind() {
		case reflect.String:
			field.SetString(val)
		case reflect.Int:
			val, err := getInt(val)
			if err != nil {
				return err
			}
			field.SetInt(val)
		case reflect.Bool:
			field.SetBool(val == "true")
		}
	}
	return nil
}

// getValue attempts to parse the first non-empty value
// from a comma-separated list of possible keys
func getValue(values string, data map[string]string) string {
	keys := strings.Split(values, ",")
	for _, k := range keys {
		if val, ok := data[k]; ok && val != "" {
			return val
		}
	}
	return ""
}

// getInt tries to parse an integer from a given
// string and safely defaults if it can't.
func getInt(val string) (int64, error) {
	if val == "" {
		return 0, nil
	}
	return strconv.ParseInt(val, 10, 64)
}
