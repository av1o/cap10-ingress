package k8s

import (
	"fmt"
	"strings"
)

type ScopedResource struct {
	Name      string
	Namespace string
}

func NewScopedResource(str string) *ScopedResource {
	bits := strings.SplitN(str, "/", 2)
	if len(bits) == 1 {
		return &ScopedResource{
			Name:      bits[0],
			Namespace: "",
		}
	}
	return &ScopedResource{
		Name:      bits[1],
		Namespace: bits[0],
	}
}

func (sr *ScopedResource) String() string {
	if sr.Namespace == "" {
		return sr.Name
	}
	return fmt.Sprintf("%s/%s", sr.Namespace, sr.Name)
}
