package k8s

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewScopedResource(t *testing.T) {
	sr := NewScopedResource("tls-foobar")
	assert.Empty(t, sr.Namespace)
	assert.EqualValues(t, "tls-foobar", sr.Name)
	assert.EqualValues(t, sr.Name, sr.String())

	sr = NewScopedResource("cert-manager/my-tls-cert")
	assert.EqualValues(t, "cert-manager", sr.Namespace)
	assert.EqualValues(t, "my-tls-cert", sr.Name)
	assert.EqualValues(t, "cert-manager/my-tls-cert", sr.String())
}
